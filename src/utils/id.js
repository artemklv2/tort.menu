function genID () {
  return `${(~~(Math.random()*1e8)).toString(20)}`
    .toUpperCase()
    .substring(0,6)
    .replace(/^(.{1,3})(.{1,3})/, '$1-$2')
}

export { genID }
