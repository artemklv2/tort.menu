function sendEvent () {
  if (
    window 
    && window.yaCounter51614387
    && window.yaCounter51614387.reachGoal
  ) {
    return window.yaCounter51614387.reachGoal(...arguments)
  }

  return function () {}
}

export { sendEvent }
