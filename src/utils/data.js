async function getCategory (code) {
  const categories = await import('../../data/categories.json').then(data => data.default)

  return categories.find(category => category.code === code)
}

async function getProductsByCategory (categoryId) {
  const products = await import('../../data/products.json').then(data => data.default)

  return products.filter(product => {
      if (product.category && product.category.id) {
        return product.category.id === categoryId
      }
      return false
    })
}

async function getProductData (code) {
  const products = await import('../../data/products.json').then(data => data.default)

  return products.find(product => product.code === code)
}

export {
  getCategory,
  getProductsByCategory,
  getProductData
}
