import { SITE_ADMIN_ORIGIN } from '../constants/rootConstants'

const API_ORDER = `${SITE_ADMIN_ORIGIN}/make_order`

async function sendOrder (type, id, order, payload) {
  const resp = await fetch(API_ORDER, {
    method: 'POST',
    mode: 'cors',
    header: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id,
      type,
      payload,
      order
    })
  })

  const json = await resp.json()

  if (json.status !== 'success') {
    throw new Error('fail to send order')
  }

  return json
}

export { sendOrder }
