const ELEMENT_DELIMITER = '__'
const MODIFIER_DELIMITER = '--'

/**
 * Класс преобразует style object в соответствии с bem  методологией наименования классов.
 *
 * @example
 * // returns 'mainMenu__item mainMenu__item--isShowed mainMenu__item--isActive'
 * // index.css
 * .mainMenu {
 *  ...
 *  &__item {
 *    &--isActive {
 *      ...
 *    }
 *    &--isShowed {
 *      ...
 *    }
 * }
 * // selector.js
 * import styles from './index.css'
 *
 * const bem = new BEM(styles)
 * bem.b('mainMenu').e('item').m('isShowed').m('isActive', true).show()
 */
class BEM {
  /**
   * @param {Object} styles
   */
  constructor (styles) {
    this.styles = styles
    this.b = this.b.bind(this)
    this.e = this.e.bind(this)
    this.m = this.m.bind(this)
    this.mix = this.mix.bind(this)
    this._reset = this._reset.bind(this)
    this.show = this.show.bind(this)
    this._reset()
  }

  _reset () {
    this.block = null
    this.element = null
    this.modifiers = []
    this.mixes = []
  }

  /**
   * @param {String} block
   */
  b (block) {
    this.block = block
    return this
  }

  /**
   * @param {String} element
   */
  e (element) {
    this.element = element
    return this
  }

  m (modifiers, display = true) {
    if (display) {
      this.modifiers.push(modifiers)
    }
    return this
  }

  /**
   * Добавляет кастомный стиль
   *
   * @param  {String} className
   */
  mix (className) {
    if (className) {
      this.mixes.push(className)
    }
    return this
  }

  /**
   * Возвращает полное название css-класса
   *
   * @returns {string}
   */
  show () {
    const baseClassKey = this.element
      ? `${this.block}${ELEMENT_DELIMITER}${this.element}`
      : this.block
    const classKeys = [
      baseClassKey,
      ...this.modifiers.map(modifier => `${baseClassKey}${MODIFIER_DELIMITER}${modifier}`)
    ]
    const mixes = this.mixes
    this._reset()
    return [...classKeys.map(classKey => this.styles[classKey]), ...mixes].join(' ')
  }
}

export { BEM }
export default BEM
