export const SITE_ADMIN_ORIGIN = 'https://admin.donnaolivia.ru'
export const IMG_PATH_ORIGIN = `${SITE_ADMIN_ORIGIN}/media/cache/sylius_shop_product_original/`
export const IMG_PATH_THUMBNAIL = `${SITE_ADMIN_ORIGIN}/media/cache/sylius_shop_product_thumbnail/`
export const IMG_PATH_THUMBNAIL_LARGE = `${SITE_ADMIN_ORIGIN}/media/cache/sylius_shop_product_large_thumbnail/`
export const IMG_PATH_THUMBNAIL_SMALL = `${SITE_ADMIN_ORIGIN}/media/cache/sylius_shop_product_small_thumbnail/`
