export const deliveryPrice = 300

const topPageGallery = [
  {
    original: '/static/images/gallery/5_1.jpg'
  },
  {
    original: '/static/images/gallery/5_2.jpg'
  },
  {
    original: '/static/images/gallery/5_3.jpg'
  },
  {
    original: '/static/images/gallery/5_4.jpg'
  },
  {
    original: '/static/images/gallery/5_5.jpg'
  },
  {
    original: '/static/images/gallery/5_7.jpg'
  },
  {
    original: '/static/images/gallery/5_8.jpg'
  }
]

const cooksGallery = [
  {
    original: '/static/images/cooks/gotovit1.jpg'
  },
  {
    original: '/static/images/cooks/gotovit2.jpg'
  },
  {
    original: '/static/images/cooks/gotovit3.jpg'
  },
  {
    original: '/static/images/cooks/gotovit4.jpg'
  },
  {
    original: '/static/images/cooks/gotovit5.jpg'
  },
  {
    original: '/static/images/cooks/gotovit6.jpg'
  },
  {
    original: '/static/images/cooks/gotovit7.jpg'
  }
]

const howOrder = [
  {
    title: 'по телефону <a href="tel:+79292048053">+7(929)204-80-53</a>',
    text: 'можно позвонить по телефону и рассказать нам, какой торт вы хотите заказать'
  },
  {
    title: 'время',
    text: `
     заказы на "быстрые" и "готовые" торты каждый день с 09:00 до 19:00, а забрать можно в любое время<br />
    `
  },
  {
    title: 'через сайт',
    text: 'выбирайте нужную вкладку <a href="/zakazat-bistrii-tort">заказать быстрый торт</a>, <a href="/quick_cake"> купить готовый торт</a> или <a href="/osobennii-tort">нужен особенный торт</a>'
  },
  {
    title: 'забрать и оплатить',
    text: 'торты можно забрать и оплатить в ресторанах Maccheroni (Ленина, 40), Mammas (Ленина, 26), Donna Olivia (8 Марта, 41) и Donna Olivia в СТЦ "МЕГА" в любое удобное для вас время'
  },
  {
    title: 'доставка',
    text: 'доставка от 250р. подробности по телефону'
  }
]

/* Готовый торт описание */
const readyCakeDescription = 'Тортик будет готов через 3 часа после заказа. Заявки принимаем с 09:00 до 19:00'

/* Быстрый торт описание */
const quickCakeDescription = 'Выбирай начинку, оформление и торт будет готов через 3 часа. Заявки принимаем с 09:00 до 19:00'

/* Быстрый торт наполнение */
const quickCakeFilling = [
  {
    id: 1,
    title: 'Африка',
    description: 'бананово- лимонный торт с шоколадной крошкой',
    image: 'africa.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 2,
    title: 'Шоколадный',
    description: 'шоколадный бисквит с муссом из темного шоколада',
    image: 'shocolade.jpg',
    price: 2250,
    weight: 1.5
  },
  {
    id: 4,
    title: 'Медовик',
    description: 'классический медовый торт с ванильным кремом',
    image: 'medovic.jpg',
    price: 2400,
    weight: 2
  },
  {
    id: 5,
    title: 'Живана',
    description: 'легкий творожный торт с вишней и бисквитом',
    image: 'zhivana.jpg',
    price: 1200,
    weight: 1,
  },
  {
    id: 8,
    title: 'Чизкейк',
    description: 'на основе сыра Филадельфия, c золотистой крочкой',
    image: 'chizkeyk.jpg',
    price: 2400,
    weight: 2
  },
  {
    id: 9,
    title: 'Морковный',
    description: 'морковный бисквит c изюмом и грецким орехом, с крем Чиз',
    image: 'morkovnyi.jpg',
    price: 1800,
    weight: 1.5
  },
  {
    id: 10,
    title: 'Эстерхази',
    description: 'ореховые коржи, заварной крем и пролине',
    image: 'esterhazi.jpg',
    price: 1500,
    weight: 1
  },
  {
    id: 11,
    title: 'Павлова',
    description: 'торт с безе, сливками и сезонными ягодами без дополнительного офрормления',
    image: 'pavlova.jpg',
    price: 3000,
    weight: 1.5,
    isCake: true,
  }
]

/* Быстрый торт оформление */
const quickCakeDecor = [
  {
    id: 1,
    image: '1_03.jpg',
    description: 'Оформление 200р - сливки, ягоды, макарун, белый шоколад',
    price: 200
  },
  {
    id: 2,
    image: '1_04.jpg',
    description: 'Оформление 660р - свежие ягоды 200гр',
    price: 660
  },
  {
    id: 3,
    image: '1_05.jpg',
    description: 'Бесплатное оформление - сливки, звёзды из мастики',
    price: 0
  },
  {
    id: 4,
    image: '1_06.jpg',
    description: 'Бесплатное оформление - шоколадная глазурь и декор',
    price: 0
  },
  {
    id: 5,
    image: '1_07.jpg',
    description: 'Бесплатное оформление - сливки, цветное драже',
    price: 0
  },
  {
    id: 6,
    image: '1_08.jpg',
    description: 'Бесплатное оформление - покрытие и украшение из сливок',
    price: 0
  },
  {
    id: 7,
    image: '1_09.jpg',
    description: 'Бесплатное оформление - сливки, безе, цветное драже',
    price: 0
  },
  {
    id: 8,
    image: '1_10.jpg',
    description: 'Бесплатное оформление - сливки',
    price: 0
  },
  {
    id: 9,
    image: '1_11.jpg',
    description: 'Оформление 660р. - свежие ягоды 200гр, шоколадные шары',
    price: 660
  },
  {
    id: 10,
    image: '1_01.jpg',
    description: ' Оформление 330р - свежие ягоды 100гр ',
    price: 330
  },
  {
    id: 11,
    image: '1_12.jpg',
    description: 'Оформление 410р - ягоды 100гр, макаронс, вафельный рожок',
    price: 410
  },
  {
    id: 12,
    image: '1_02.jpg',
    description: 'Оформление 330р - свежие ягоды 100гр',
    price: 330
  }
]

/* Особенный торт описание */
const specialCakeDescription = 'Нужен особенный торт? Выберете начинку, придумайте оформление или расскажите нам, каким он должен быть. Приготовим за 1-3 дня, подробности по телефону.'

/* Особенный торт наполнение */
const specialCakeFilling = [
  {
    id: 19,
    title: 'Красный бархат',
    description: 'классический торт красный бархат с кремом Чиз',
    image: 'photo_2020-08-05_09-39-56.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 1,
    title: 'Африка',
    description: 'бананово- лимонный торт с шоколадной крошкой',
    image: 'africa.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 2,
    title: 'Шоколадный',
    description: 'шоколадный бисквит, с муссом из тёмного шоколада',
    image: 'shocolade.jpg',
    price: 1500,
    weight: 1
  },
  {
    id: 3,
    title: 'Наташа',
    description: 'три бисквитных коржа с орехом, изюмом и маком',
    image: 'natasha.png',
    price: 1200,
    weight: 1
  },
  {
    id: 4,
    title: 'Медовик',
    description: 'классический медовый торт с ванильным кремом',
    image: 'medovic.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 5,
    title: 'Живана',
    description: 'легкий творожный торт с вишней и бисквитом',
    image: 'zhivana.jpg',
    price: 1200,
    weight: 1,
  },
  {
    id: 6,
    title: 'Птичье молоко',
    description: 'нежное суфле покрытое шоколадной глазурью',
    image: 'milk.jpg',
    price: 900,
    weight: 1
  },
  {
    id: 7,
    title: 'Черемуховый',
    description: 'черемуховые коржи со сметанным кремом',
    image: 'cheremucha.jpg',
    price: 700,
    weight: 1
  },
  {
    id: 8,
    title: 'Чизкейк',
    description: 'на основе сыра Филадельфия, c золотистой крочкой',
    image: 'chizkeyk.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 11,
    title: 'Мусс эль манго',
    description: 'манговый мусс с добавлением шоколада на песочной основе',
    image: 'mys_el_mango.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 13,
    title: 'Павлова',
    description: 'воздушное безе, взбитые сливки и свежие ягоды',
    image: 'pavlova.jpg',
    price: 3000,
    weight: 1.5
  },
  {
    id: 14,
    title: 'Прага',
    description: 'шоколадный бисквит со сливочным кремом',
    image: 'praga.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 15,
    title: 'Венецианская карамель',
    description: 'карамельный мусс с шоколадным бисквитом',
    image: 'ven_karamel.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 16,
    title: 'Венера',
    description: 'коржи из слоеного теста с заварным кремом',
    image: 'napoleon.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 17,
    title: 'Морковный',
    description: 'морковный бисквит c изюмом и грецким орехом, с крем Чиз',
    image: 'morkovnyi.jpg',
    price: 1200,
    weight: 1
  },
  {
    id: 18,
    title: 'Эстерхази',
    description: 'ореховые коржи, заварной крем и пролине',
    image: 'esterhazi.jpg',
    price: 1500,
    weight: 1
  }
]

/* Особенный торт оформление */
const specialCakeDecor = [
  {
    id: 16,
    image: '16.jpg',
    
  },
  {
    id: 17,
    image: '17.jpg',
    
  },
  {
    id: 18,
    image: '18.jpg',
    
  },
  {
    id: 1,
    image: '01.jpg',
    
  },
  {
    id: 2,
    image: '02.jpg',
    
  },
  {
    id: 3,
    image: '03.jpg',
    
  },
  {
    id: 4,
    image: '04.jpg',
    
  },
  {
    id: 5,
    image: '05.jpg',
    
  },
  {
    id: 6,
    image: '06.jpg',
   
  },
  {
    id: 7,
    image: '07.jpg',
   
  },
  {
    id: 8,
    image: '08.jpg',
  
  },
  {
    id: 9,
    image: '09.jpg',
    
  },
  {
    id: 10,
    image: '10.jpg',
    
  },
  {
    id: 11,
    image: '11.jpg',
   
  },
  {
    id: 12,
    image: '12.jpg',
    
  },
  {
    id: 13,
    image: '13.jpg',
    
  },
  {
    id: 14,
    image: '14.jpg',
  
  },
  {
    id: 15,
    image: '15.jpg',
    
  }
]

export {
  readyCakeDescription,
  quickCakeDescription,
  quickCakeFilling,
  quickCakeDecor,
  specialCakeDescription,
  specialCakeFilling,
  specialCakeDecor,
  topPageGallery,
  cooksGallery,
  howOrder
}
