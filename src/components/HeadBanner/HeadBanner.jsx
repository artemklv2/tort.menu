import React from 'react'
import { BEM } from '../../utils/bem';
import styles from './HeadBanner.css'

const bem = new BEM(styles)

class HeadBanner extends React.Component {
  render() {
    return (
      <div className={bem.b('HeadBanner').show()}>
        <a href="/">
          <img src="/static/images/logo3.svg" />
        </a>
      </div>
    )
  }
}

export { HeadBanner }
