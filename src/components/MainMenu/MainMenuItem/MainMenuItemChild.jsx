import React from 'react'
import styles from './MainMenuItemChild.css'
import { BEM } from '../../../utils/bem'

const bem = new BEM(styles)

function MainMenuItemChild ({href, pathname, children}) {
  const isActive = href.split('/')[1] === pathname.split('/')[1]

  return (
    <a
      href={href}
      className={bem.b('MainMenuItemChild').m('isActive', isActive).show()}
    >
      {children}
    </a>
  )
}

export { MainMenuItemChild }
