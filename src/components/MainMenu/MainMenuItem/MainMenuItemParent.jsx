import React from 'react'
import PropTypes from 'prop-types'
import { MainMenuItemBase } from './MainMenuItemBase'
import styles from './MainMenuItemParent.css'
import { BEM } from '../../../utils/bem'

const bem = new BEM(styles)

function checkIsActive (pathname, children) {
  return React.Children
    .toArray(children)
    .filter(child => child.props.href)
    .some(child => child.props.href.split('/')[1] === pathname.split('/')[1])
}

class MainMenuItemParent extends React.Component {
  state = {
    isOpen: false,
    isActive: false
  }

  static propTypes = {
    title: PropTypes.string
  }

  handleOnClick = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  componentDidMount() {
    this.setState({
      isActive: checkIsActive(this.props.pathname, this.props.children)
    })
  }

  render () {
    const { isOpen, isActive } = this.state
    const { title, children } = this.props

    return (
      <MainMenuItemBase
        Component="button"
        onClick={this.handleOnClick}
        className={bem.b('MainMenuItemParent').show()}
        isActive={isActive}
      >
        <span className={bem.b('MainMenuItemParent').e('title').show()}>{title}</span>
        {isOpen && (
          <div className={bem.b('MainMenuItemParent').e('submenu').show()}>
            {children}
          </div>
        )}
      </MainMenuItemBase>
    )
  }
}

export { MainMenuItemParent }
