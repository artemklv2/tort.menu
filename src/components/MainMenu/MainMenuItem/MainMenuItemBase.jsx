import React from 'react'
import PropTypes from 'prop-types'
import styles from './MainMenuItemBase.css'
import BEM from '../../../utils/bem'

const bem = new BEM(styles)

function MainMenuItemBase ({Component, children, className, isActive, ...rest}) {
  return (
    <Component
      className={bem.b('MainMenuItemBase').m('isActive', isActive).mix(className).show()}
      {...rest}
    >
      {children}
    </Component>
  )
}

MainMenuItemBase.propTypes = {
  Component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.string,
    PropTypes.instanceOf(React.Component)
  ]),
  isActive: PropTypes.bool
}

MainMenuItemBase.defaultProps = {
  isActive: false
}

export { MainMenuItemBase }
