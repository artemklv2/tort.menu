import React from 'react'
import { MainMenuItemBase } from './MainMenuItemBase'

function MainMenuItemLink ({children, pathname, href, ...rest}) {
  const isActive = href.split('/')[1] === pathname.split('/')[1]
  return (
    <MainMenuItemBase
      Component="a"
      href={href}
      isActive={isActive}
      {...rest}
    >
      {children}
    </MainMenuItemBase>
  )
}

export { MainMenuItemLink }
