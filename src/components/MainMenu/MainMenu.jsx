import React from 'react'
import PropTypes from 'prop-types'
import { MainMenuItemLink, MainMenuItemChild, MainMenuItemParent } from './MainMenuItem'
import styles from './MainMenu.css'
import BEM from '../../utils/bem'
import { MainMenuIcons } from './MainMenuIcons'
import { MainMenuBurger } from './MainMenuBurger'
import { sendEvent } from '../../utils/yandex'
import { LayoutInner } from '../Layout'

const bem = new BEM(styles)

class MainMenu extends React.Component {
  state = {
    isMenuOpen: false
  }

  static propTypes = {
    pathname: PropTypes.string
  }

  handleOnBurgerClick = () => {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    })
  }

  render () {
    const { isMenuOpen } = this.state
    const { pathname } = this.props

    return (
      <nav className={bem.b('MainMenu').show()}>
        <MainMenuBurger
          isOpen={isMenuOpen}
          onClick={this.handleOnBurgerClick}
        />
        <LayoutInner className={bem.b('MainMenu').e('inner').show()}>
          <MainMenuIcons />
          <div className={bem.b('MainMenu').e('logo').show()}>
            <a href="/">
              <img height="60px"  src="/static/images/logo3.svg" />
            </a>
          </div>
          <div className={bem.b('MainMenu').e('items').m('isOpen', isMenuOpen).show()}>
            <MainMenuItemParent
              title="заказать торт"
              pathname={pathname}
            >
              <MainMenuItemChild pathname={pathname} href="/zakazat-bistrii-tort">торт за 3 часа</MainMenuItemChild>
              <div className={bem.b('MainMenu').e('submenuDelimiter').show()} />
              <MainMenuItemChild pathname={pathname} href="/osobennii-tort">собери свой торт</MainMenuItemChild>
              <div className={bem.b('MainMenu').e('submenuDelimiter').show()} />
            </MainMenuItemParent>
            <MainMenuItemLink pathname={pathname} href="/nashi-torty">наши торты</MainMenuItemLink>
            <MainMenuItemLink pathname={pathname} href="/kto-gotovit">кто готовит</MainMenuItemLink>
          </div>
          <a
            href="tel:+79292048053"
            className={bem.b('MainMenu').e('phone').show()}
            onClick={() => sendEvent('tel')}
          >
            +7 (929) 204 80-53
          </a>
        </LayoutInner>
      </nav>
    )
  }
}

export { MainMenu }
