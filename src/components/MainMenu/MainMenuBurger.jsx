import React from 'react'
import PropTypes from 'prop-types'
import styles from './MainMenuBurger.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles)

function MainMenuBurger ({
  onClick,
  isOpen
}) {
  return (
    <button
      className={bem.b('MainMenuBurger').show()}
      onClick={onClick}
    >
      <span className={bem.b('MainMenuBurger').e('line').m('one').m('isOpen', isOpen).show()} />
      <span className={bem.b('MainMenuBurger').e('line').m('two').m('isOpen', isOpen).show()} />
      <span className={bem.b('MainMenuBurger').e('line').m('three').m('isOpen', isOpen).show()} />
    </button>
  )
}

MainMenuBurger.defaultProps = {
  onClick: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
}

export { MainMenuBurger }
