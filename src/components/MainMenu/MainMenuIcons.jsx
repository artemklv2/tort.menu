import React from 'react'
import styles from './MainMenuIcons.css'
import { BEM } from '../../utils/bem'
import { sendEvent } from '../../utils/yandex'
import { IconInstagram } from './IconInstagram'
import { IconWhatsapp } from './IconWhatsapp'
import { IconPhone } from './IconPhone'
import { CardCount } from '../Card'

const bem = new BEM(styles)


function MainMenuIcons () {
  return (
    <div
      className={bem.b('MainMenuIcons').show()}
    >
      <a
        href="https://www.instagram.com/tort.menu/"
        className={bem.b('MainMenuIcons').e('icon').show()}
        onClick={() => sendEvent('instagram')}
      >
        <IconInstagram className={bem.b('MainMenuIcons').e('iconSVG').show()} />
      </a>
      <a
        href="https://wa.me/79292048053"
        className={bem.b('MainMenuIcons').e('icon').show()}
        onClick={() => sendEvent('whatsapp')}
      >
        <IconWhatsapp className={bem.b('MainMenuIcons').e('iconSVG').show()} />
      </a>
      <a
        href="tel:+79292048053"
        
        className={bem.b('MainMenuIcons').e('icon').m('phone').show()}
        onClick={() => sendEvent('whatsapp')}
      >
        <IconPhone className={bem.b('MainMenuIcons').e('iconSVG').show()} />
      </a>
      <CardCount />
    </div>
  )
}

export { MainMenuIcons }
