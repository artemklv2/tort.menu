import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import { IMG_PATH_THUMBNAIL } from '../../constants/rootConstants'
import { LayoutInner } from '../Layout'
import styles from './CategorySlider.css'

const settings = {
  className: styles.CategorySlider,
  infinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 1,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 361,
      settings: {
        slidesToShow: 1
      }
    }
  ]
}

class CategorySlider extends React.Component {
  static propTypes = {
    products: PropTypes.array.isRequired,
    parentUrl: PropTypes.string.isRequired
  }

  render () {
    const {
      products,
      parentUrl
    } = this.props

    return (
      <LayoutInner>
        <Slider
          {...settings}
        >
          {products.map(({id, code, images, name, price}) => (
            <a
              key={`product${id}`}
              href={`${parentUrl}/${code}`}
              className={styles.CategorySlider__item}
            >
              <img
                alt={name}
                src={`${IMG_PATH_THUMBNAIL}${images[0]}`}
              />
              <p
                className={styles.CategorySlider__itemValue}
              >
                <span
                  className={styles.CategorySlider__itemTitle}
                >
                  {name}
                </span>
                <span
                  className={styles.CategorySlider__itemPrice}
                >
                  {new Intl.NumberFormat('ru-RU').format(price)} ₽
                </span>
              </p>
            </a>
          ))}
        </Slider>
      </LayoutInner>
    )
  }
}

export { CategorySlider }
