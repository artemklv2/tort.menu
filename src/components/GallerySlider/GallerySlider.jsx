import React from 'react'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import BEM from '../../utils/bem'
import styles from './GallerySlider.css'

const bem = new BEM(styles)

function GallerySlider ({items}) {
  return (
    <div className={bem.b('GallerySlider').show()}>
      <Slider
        autoplay={true}
        dots={false}
        fade={true}
        autoplaySpeed={2000}
        speed={2000}
        style={{
          height: '600px'
        }}
        responsive={[
          {
            breakpoint: 361,
            settings: {
              slidesToShow: 1
            }
          }
        ]}
      >
        {items.map((item, index) => (
          <div
            key={index}
            className={bem.b('GallerySlider').e('item').show()}
          >
            <img
              className={bem.b('GallerySlider').e('image').show()}
              src={item.original}
            />
          </div>

        ))}
      </Slider>
    </div>
  )
}

GallerySlider.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      original: PropTypes.string
    })
  )
}

export {
  GallerySlider
}
