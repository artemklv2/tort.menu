import React from 'react'
import PropTypes from 'prop-types'
import BEM from '../../utils/bem'
import styles from './OrderResultHandler.css'

const bem = new BEM(styles);

function OrderResultHandler ({isSubmitted, hasError, children}) {
  return (
    <>
      {hasError ? (
        <>
          <h1 className={bem.b('OrderResultHandler').e('title').show()}>Ошибка отправки заявки!</h1>
          <p className={bem.b('OrderResultHandler').e('message').show()}><a href="tel:7-343-222-1902">Позвоните нам</a> и мы примем заказ по телефону</p>
        </>
      ) : (
        isSubmitted ? (
          <>
            <h1 className={bem.b('OrderResultHandler').e('title').show()}>Ваша заявка отправлена!</h1>
            <p className={bem.b('OrderResultHandler').e('message').show()}>В ближайшее время мы вам перезвоним и уточним детали заявки</p>
          </>
        ) : (
          children
        )
      )}
    </>
  )
}

OrderResultHandler.propTypes = {
  hasError: PropTypes.bool,
  isSubmitted: PropTypes.bool
}

export default OrderResultHandler
