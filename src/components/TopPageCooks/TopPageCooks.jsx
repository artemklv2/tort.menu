import React from 'react'
import { cooksGallery } from '../../data'
import Slider from 'react-slick'
import styles from './TopPageCooks.css'

function TopPageCooks () {
  return (
    <div className={styles.TopPageCooks}>
      <div
        className={styles.TopPageCooks__text}
      >
        <p
          className={styles.TopPageCooks__textInner}
        >
         Почему все любят наши тортики? Потому что у нас вкусные рецепты. 
         Потому что мы используем только качественные и натуральные продукты, 
         обходимся без консервантов, печём на современном оборудовании и любим свое дело. 
         Получается вкусно и красиво!
        </p>
      </div>

      <div
        className={styles.TopPageCooks__gallery}
      >
        <Slider
          autoplay={true}
          dots={false}
          fade={true}
          autoplaySpeed={2000}
          speed={2000}
          arrows={false}
        >
          {cooksGallery.map((item, index) => (
            <img key={`img${index}`} src={item.original} />
          ))}
        </Slider>
      </div>
    </div>
  )
}

export {
  TopPageCooks
}
