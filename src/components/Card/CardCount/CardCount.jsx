import React from 'react'
import PropTypes from 'prop-types'
import { withCard } from '../Context'
import { Icon } from './Icon'
import styles from './CardCount.css'

class CardCount extends React.Component {
  static propTypes = {
    card: PropTypes.object
  }

  render () {
    const count = this.props.card.products.length 
    if (Boolean(count) === false) {
      return null;
    }
    
    return (
      <a
        href={'/card'}
        className={styles.CardCount}
      >
        <Icon>{count}</Icon>
      </a>
    )
  }
}

export default withCard(CardCount)
