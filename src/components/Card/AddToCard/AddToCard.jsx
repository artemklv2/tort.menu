import React from 'react'
import PropTypes from 'prop-types'
import { withCard } from '../Context'
import styles from './AddToCard.css'
import BEM from '../../../utils/bem'
import { sendEvent } from '../../../utils/yandex.js'

const bem = new BEM(styles)

function checkProduct (card, productId) {
  return card.products.some(item => (item.product.id === productId))
}

class AddToCard extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired,
    card: PropTypes.object.isRequired,
    addProductToCard: PropTypes.func.isRequired
  }

  state = {
    isProductAdded: false,
    count: 1
  }

  componentDidMount () {
    this.setState({
      isProductAdded: checkProduct(this.props.card, this.props.product.id)
    })
  }

  componentDidUpdate (prevProps) {
    if (prevProps.card !== this.props.card) {
      this.setState({
        isProductAdded: checkProduct(this.props.card, this.props.product.id)
      })
    }
  }

  handleOnClick = () => {
    const {
      product,
      addProductToCard
    } = this.props
    
    sendEvent('addtocard')
    addProductToCard(product, this.state.count)
  }

  handleOnCountChange = (event) => {
    this.setState({
      count: Number(event.target.value)
    })
  }

  render () {
    const { isProductAdded, count } = this.state
    const { product } = this.props

    return (
      <div className={bem.b('AddToCard').m('added', isProductAdded).show()}>
        {isProductAdded ? (
          <>
            <a
              className={bem.b('AddToCard').e('link').m('back').show()}
              href={`/${product.category.code}`}
            >Продолжить покупки</a>
            <a
              className={bem.b('AddToCard').e('link').m('order').show()}
              href="/card"
            >Оформить заявку</a>
          </>
        ) : (
          <>
            <input
              className={bem.b('AddToCard').e('count').show()}
              type="number"
              value={count}
              onChange={this.handleOnCountChange}
            />
            <button
              className={bem.b('AddToCard').e('add').show()}
              onClick={this.handleOnClick}
            >
              Добавить в корзину
            </button>
          </>
        )}
      </div>
    )
  }
}

export default withCard(AddToCard)
