import React from 'react'

const initCard = {
  products: [],
  order: {}
}

const { Consumer, Provider } = React.createContext()

class CardProvider extends React.Component {
  state = {
    card: initCard
  }

  componentDidMount () {
    const data = window.localStorage.getItem('card')
    if (data) {
      try {
        const card = JSON.parse(data)
        this.setState({card})
      } catch (e) {

      }
    }
  }

  addProductToCard = (product, count) => {
    const products = [
      ...this.state.card.products,
      {
        product,
        count
      }
    ]

    const card = {
      ...this.state.card,
      products
    }

    this.setState({
      card
    }, () => {
      window.localStorage.setItem('card', JSON.stringify(card))
    })
  }

  removeProductFromCard = (productId) => {
    const products = this.state.card.products.filter(item => item.product.id !== productId)

    const card = {
      ...this.state.card,
      products
    }

    this.setState({
      card
    }, () => {
      window.localStorage.setItem('card', JSON.stringify(card))
    })
  }

  changeProductCountInCard = (productId, count) => {
    const products = this.state.card.products.map(item => {
      if (item.product.id === productId) {
        item.count = count
      }
      return item
    })

    const card = {
      ...this.state.card,
      products
    }

    this.setState({
      card
    }, () => {
      window.localStorage.setItem('card', JSON.stringify(card))
    })
  }

  removeCard = () => {
    this.setState({
      card: {
        products: [],
        order: {}
      }
    }, window.localStorage.clear())
  }

  setOrder = (order) => {
    const card = {
      ...this.state.card,
      order
    }

    this.setState({
      card
    }, () => {
      window.localStorage.setItem('card', JSON.stringify(card))
    })
  }

  render () {
    const { card } = this.state

    return (
      <Provider
        value={{
          card,
          addProductToCard: this.addProductToCard,
          removeProductFromCard: this.removeProductFromCard,
          changeProductCountInCard: this.changeProductCountInCard,
          setOrder: this.setOrder,
          removeCard: this.removeCard
        }}
      >
        {this.props.children}
      </Provider>
    )
  }
}

function withCard(WrappedComponent) {
  return class WithCard extends React.Component {
    static displayName = WrappedComponent.displayName || 'WithCard'

    render () {
      return (
        <Consumer>
          {value => (
            <WrappedComponent
              {...this.props}
              {...value}
            />
          )}
        </Consumer>
      )
    }
  }
}

export {
  CardProvider,
  withCard
}
