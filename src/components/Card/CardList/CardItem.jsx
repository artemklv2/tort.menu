import React from 'react'
import {withCard} from '../Context'
import PropTypes from 'prop-types'
import styles from './CardItem.css'
import { IMG_PATH_THUMBNAIL_SMALL } from "../../../constants/rootConstants";

class CardItem extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired
  }

  handleOnRemove = () => {
    const {
      product,
      removeProductFromCard
    } = this.props

    removeProductFromCard(product.id)
  }

  handleOnCountChange = (event) => {
    const {
      product,
      changeProductCountInCard
    } = this.props

    const count = Math.max(Number(event.target.value), 1)

    changeProductCountInCard(product.id, count)
  }

  render () {
    const { product: {name, images, price}, count } = this.props
    const sum = price * count

    return (
      <div className={styles.CardItem}>
        <div className={styles.CardItem__image}>
          {images.length && (
            <img src={`${IMG_PATH_THUMBNAIL_SMALL}${images[0]}`} />
          )}
        </div>
        <div className={styles.CardItem__name}>
          {name}
        </div>
        <div className={styles.CardItem__price}>
          {new Intl.NumberFormat('ru-RU').format(price)} ₽
        </div>
        <div className={styles.CardItem__count}>
          <input
            className={styles.CardItem__countInput}
            type="number"
            value={count}
            onChange={this.handleOnCountChange}
          />
          <button
            className={styles.CardItem__countDelete}
            onClick={this.handleOnRemove}
          >x</button>
        </div>
        <div className={styles.CardItem__sum}>
          {new Intl.NumberFormat('ru-RU').format(sum)} ₽
        </div>
      </div>
    )
  }
}

export default withCard(CardItem)
