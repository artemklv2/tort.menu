import React from 'react'
import { withCard } from '../Context'
import CardItem from './CardItem'
import styles from './CardList.css'
import { OrderForm } from '../../OrderForm'
import { sendOrder } from '../../../utils/api'
import { sendEvent } from '../../../utils/yandex'

class CardList extends React.Component {
  state = {
    order: {},
    isFetching: false,
    isSubmitted: false,
    hasError: false
  }

  constructor (props) {
    super(props)
    this.submit = this.submit.bind(this)
    this.setIsFetching = this.setIsFetching.bind(this)
    this.setIsSubmitted = this.setIsSubmitted.bind(this)
    this.setHasError = this.setHasError.bind(this)
  }

  setIsFetching (value) {
    return new Promise((resolve) => {
      this.setState({
        isFetching: value
      }, () => resolve(value))
    })
  }

  setIsSubmitted (value) {
    return new Promise((resolve) => {
      this.setState({
        isSubmitted: value
      }, () => resolve(value))
    })
  }

  setHasError (value) {
    return new Promise((resolve) => {
      this.setState({
        hasError: value
      }, () => resolve(value))
    })
  }

  setOrder = (order) => {
    this.setState({
      order
    })
  }

  async submit (id, order, form) {
    const {
      card: {
        products
      },
      removeCard
    } = this.props

    sendEvent('ordertort')

    await this.setIsFetching(true)

    try {
      await sendOrder('card', id, order, products)
      await removeCard()
      if (order.payment === 'online') {
        form.submit()
      }
    } catch (error) {
      this.setHasError(true)
    } finally {
      await this.setIsFetching(false)
      await this.setIsSubmitted(true)
    }
  }

  render () {
    const { card: { products } } = this.props
    const { order, isFetching, hasError, isSubmitted } = this.state

    const total = products.reduce((acc, {product, count}) => {
      acc += product.price * count
      return acc
    }, 0)

    return (
      <div className={styles.CardList}>
        {hasError ? (
          <div className={styles.CardList__error}>
            <h1 className={styles.CarList__successMessageTitle}>Ошибка отправки заявки</h1>
            <p><a href="tel:7-343-222-1902">Позвоните нам</a> и мы примем заказ по телефону</p>
          </div>
        ) : (
          isSubmitted ? (
            <>
              <h1 className={styles.CarList__successMessageTitle}>Ваша заявка отправлена!</h1>
              <p>В ближайшее время мы вам перезвоним и уточним детали заявки</p>
            </>
          ) : (
            <>
              <h1 className={styles.CardList__title}>Ваш заказ</h1>
              <div className={styles.CardList__items}>
                {products.map(item => (
                  <CardItem
                    key={`product${item.product.id}`}
                    {...item}
                  />
                ))}
              </div>
              <div className={styles.CardList__total}>
                Итого: {new Intl.NumberFormat('ru-RU').format(total)} ₽
              </div>
              <h1 className={styles.CardList__title}>Подробности заказа</h1>
              <OrderForm
                total={total}
                reset={() => null}
                order={order}
                setOrder={this.setOrder}
                submit={this.submit}
                submitTitle="Отравить заявку"
                isFetching={isFetching}
                isValid={true}
              />
            </>
          )
        )}
      </div>
    )
  }
}

export default withCard(CardList)
