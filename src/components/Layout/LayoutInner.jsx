import React from 'react'
import styles from './LayoutInner.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles)

const LayoutInner = ({children, className}) => (
  <div className={bem.b('LayoutInner').mix(className).show()}>
    {children}
  </div>
)

export { LayoutInner }
