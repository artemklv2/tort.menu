import React from 'react'
import PropTypes from 'prop-types'
import { HeadBanner } from '../HeadBanner'
import { MainMenu } from '../MainMenu'
import Head from 'next/head'
import styles from './Layout.css'
import { CardProvider } from '../Card'

const yandexCounter = `
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(51614387, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51614387" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
`

const googleTagManager = `
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TPF4D9X');
`

class Layout extends React.Component {
  static propTypes = {
    pathname: PropTypes.string.isRequired,
  }

  componentDidMount () {
    if (window) {
      const urlParams = new URLSearchParams(window.location.search)
      const source = urlParams.get('source')
      if (source) {
        window.sessionStorage.setItem('source', source)
      }
    }
  }

  render () {
    const { children, pathname } = this.props

    return (
      <CardProvider>
        <Head>
          <meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1'/>
          <meta charSet="utf-8" />
          <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
          <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
          <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
          <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet"></link>
          <script dangerouslySetInnerHTML={{__html: googleTagManager}} />
          <script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>  
        
        </Head>
        <div className={styles.Layout__header}>
          <div className={styles.Layout__headerInner}>
            <HeadBanner />
            <MainMenu pathname={pathname} />
          </div>
        </div>
        {children}
        <div  dangerouslySetInnerHTML={{__html: yandexCounter}} />
      </CardProvider>
    )
  }
}

export { Layout }
