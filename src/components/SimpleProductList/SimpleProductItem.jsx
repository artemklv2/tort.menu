import React from 'react'
import PropTypes from 'prop-types'
import styles from './SimpleProductItem.css'
import BEM from '../../utils/bem'
import { SITE_ADMIN_ORIGIN } from '../../constants/rootConstants'

const bem = new BEM(styles)

class SimpleProductItem extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    images: PropTypes.array,
    id: PropTypes.number.isRequired,
    price: PropTypes.number,
    code: PropTypes.string,
    category: PropTypes.object.isRequired
  }

  state = {
    currentImageIndex: 0
  }

  handleOnMouseEnter = () => {
    if (this.props.images.length > 1) {
      this.setState({
        currentImageIndex: 1
      })
    }
  }

  handleOnMouseLeave = () => {
    this.setState({
      currentImageIndex: 0
    })
  }

  render () {
    const {
      images,
      name,
      price,
      code,
      parentUrl
    } = this.props

    const {
      currentImageIndex
    } = this.state

    return (
      <a
        href={`${parentUrl}/${code}`}
        className={styles.SimpleProductItem}
        onMouseEnter={this.handleOnMouseEnter}
        onMouseLeave={this.handleOnMouseLeave}
      >
        <div className={bem.b('SimpleProductItem').e('imageList').show()}>
          <img
            src={`${SITE_ADMIN_ORIGIN}/media/cache/sylius_shop_product_thumbnail/${images[currentImageIndex]}`}
            className={bem.b('SimpleProductItem').e('imageItem').show()}
          />
        </div>
        <span className={bem.b('SimpleProductItem').e('name').show()}>
          {name}
        </span>
        <span className={bem.b('SimpleProductItem').e('price').show()}>
          {new Intl.NumberFormat('ru-RU').format(price)} ₽
        </span>
      </a>
    )
  }
}

export { SimpleProductItem }
