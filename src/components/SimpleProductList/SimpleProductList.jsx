import React from 'react'
import PropTypes from 'prop-types'
import { SimpleProductItem } from './SimpleProductItem'
import { LayoutInner } from '../Layout'
import styles from './SimpleProductList.css'


class SimpleProductList extends React.Component {
  static propTypes = {
    products: PropTypes.array.isRequired
  }

  render () {
    const { products, parentUrl } = this.props

    return (
      <LayoutInner>
        <div className={styles.SimpleProductList}>
          {products.map(product => (
            <SimpleProductItem
              key={`product${product.id}`}
              parentUrl={parentUrl}
              {...product}
            />
          ))}
        </div>
      </LayoutInner>
    )
  }
}

export { SimpleProductList }
