import React from 'react'
import PropTypes from 'prop-types'
import BEM from '../../utils/bem'
import styles from './BreadCrumb.css'

const bem = new BEM(styles)

class BreadCrumb extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired
  }

  render () {
    const { product, category } = this.props

    return (
      <div className={bem.b('BreadCrumb').show()}>
        <a className={bem.b('BreadCrumb').e('item').show()} href="/">
          Главная
        </a>
        &nbsp;/&nbsp;
        <a className={bem.b('BreadCrumb').e('item').show()} href={`/${category.code}`}>
          {category.name}
        </a>
        &nbsp;/&nbsp;
        <span className={bem.b('BreadCrumb').e('item').m('disabled').show()}>
          {product.name}
        </span>
      </div>
    )
  }
}

export { BreadCrumb }
