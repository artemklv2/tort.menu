import React from 'react'
import PropTypes from 'prop-types'
import { BreadCrumb } from './BreadCrumb'
import { LayoutInner } from '../Layout'
import { ProductGallery } from './ProductGallery'
import styles from './ProductPage.css'
import { AddToCard } from '../Card'

function Attributes ({attributes}) {
  if (!attributes.length) {
    return null
  }

  return `(${attributes.map((attr) => {
    return `${attr.name} - ${attr.value}${attr.code === 'weight' ? 'г.' : ''}`
  }).join(', ')})`
}

class ProductPage extends React.Component {
  static propTypes = {
    product: PropTypes.object.isRequired
  }

  render () {
    const { product } = this.props

    return (
      <LayoutInner>
        <BreadCrumb product={product} category={product.category} />
        <div className={styles.ProductPage__block}>

          <ProductGallery images={product.images} />

          <div className={styles.ProductPage__info}>
            <div className={styles.ProductPage__infoInner}>
              <h1 className={styles.ProductPage__title}>
                {product.name}
                &nbsp; <span className={styles.ProductPage__titleInfo}>
                <Attributes attributes={product.attributes} />
              </span>
              </h1>
              {product.description && (
                <p className={styles.ProductPage__description}>
                  {product.description}
                </p>
              )}
              <p className={styles.ProductPage__price}>
                {new Intl.NumberFormat('ru-RU').format(product.price)} ₽
              </p>
            </div>

            <AddToCard product={product} />

          </div>
        </div>
      </LayoutInner>
    )
  }
}

export { ProductPage }
