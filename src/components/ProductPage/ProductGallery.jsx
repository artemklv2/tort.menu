import React from 'react'
import PropTypes from 'prop-types'
import 'react-image-gallery/styles/css/image-gallery.css'
import ImageGallery from 'react-image-gallery'
import {
  IMG_PATH_THUMBNAIL_SMALL,
  IMG_PATH_THUMBNAIL_LARGE
} from '../../constants/rootConstants'
import styles from './ProductGallery.css'

class ProductGallery extends React.Component {
  static propTypes = {
    images: PropTypes.array.isRequired
  }

  state = {
    images: []
  }

  componentDidMount () {
    const images = this.props.images.map(image => ({
      original: `${IMG_PATH_THUMBNAIL_LARGE}${image}`,
      thumbnail: `${IMG_PATH_THUMBNAIL_SMALL}${image}`
    }))
    this.setState({
      images
    })
  }

  render () {
    const { images } = this.state

    if (!images.length) {
      return null
    }

    return (
      <ImageGallery
        additionalClass={styles.ProductGallery}
        items={images}
        showNav={false}
        showPlayButton={false}
        showThumbnails={images.length > 1}
      />
    )
  }
}

export { ProductGallery }
