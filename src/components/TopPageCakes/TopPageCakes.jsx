import React from 'react'
import styles from './TopPageCakes.css'

function TopPageCakes () {
  return (
    <section className={styles.TopPageCakes}>
      <div className={styles.TopPageCakes__inner}>
        <div className={styles.TopPageCakes__list}>
          <a href="/zakazat-bistrii-tort" className={styles.TopPageCakes__item}>
            <div className={styles.TopPageCakes__itemText}>
              <h3>- ТОРТ ЗА 3 ЧАСА -</h3>
              <p>
                очень просто<br />
                шаг 1 - выбираете начинку<br />
                шаг 2 - выбираете оформление<br />
                шаг 3 - через 3 часа<br />
                можно забрать тортик
              </p>
            </div>
          </a>

          <a href="/osobennii-tort/" className={styles.TopPageCakes__item}>
            <div className={styles.TopPageCakes__itemText}>
              <h3>-СОБЕРИ СВОЙ ТОРТ -</h3>
              <p>
                приготовим за 1-3 дня <br />
                шаг 1 - выбираете начинку<br />
                шаг 2 - придумайте оформление<br />
                шаг 3 - тортик готов к вашему празднику<br />
              </p>
            </div>
          </a>
        </div>
      </div>
    </section>
  )
}

export { TopPageCakes }
