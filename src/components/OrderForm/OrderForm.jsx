import React from 'react'
import PropTypes from 'prop-types'
import styles from './OrderForm.css'
import BEM from '../../utils/bem'
import FormField from './FormField'
import PhoneInput from './PhoneInput'
import DateInput from './DateInput'
import dateFormat from 'date-fns/format'
import { OrderInfo } from './OrderInfo'
import { genID } from '../../utils/id'
import { deliveryPrice } from '../../data';

const bem = new BEM(styles)

const SHOP_ID = '603440';
const SC_ID = '938046';

function getValues (values) {
  return values.reduce((acc, name) => {
    acc[name] = {
      value: '',
      error: '',
      touched: false,
      focused: false
    }
    return acc
  }, {})
}

function validate (values) {
  values.name.error = null
  values.phone.error = null
  values.comment.error = null
  values.date.error = null
  let error = false

  if (!values.name.value) {
    values.name.error = 'Не указано имя'
    error = true
  }
  if (values.name.value.length < 3) {
    values.name.error = 'Имя не может быть меньше трех символов'
    error = true
  }
  if (values.phone.value.replace(/\D/g, '').length < 11) {
    values.phone.error = 'Неправильно указан телефон'
    error = true
  }
  if (!values.date.value) {
    values.date.error = 'Не указана дата'
    error = true
  }

  return {
    values,
    error
  }
}

class OrderForm extends React.Component {
  state = {
    error: null,
    values: getValues(this.props.values),
    delivery: 'pickup',
    payment: 'cash',
    id: genID()
  }

  handleOnReset = () => {
    this.setState({
      delivery: 'pickup',
      payment: 'cash'
    })
    if (typeof this.props.reset === 'function') {
      this.props.reset()
    }
  }

  handleOnDeliveryChange = (value) => {
    this.setState({
      delivery: value
    })
  }

  handleOnPaymentChange = (value) => {
    this.setState({
      payment: value
    })
  }

  handleOnChange = (event) => {
    const { value, name } = event.target

    const values = Object.entries(this.state.values)
      .map(([key, option]) => {
        if (key === name) {
          option.value = value
        }
        return [key, option]
      })
      .reduce((acc, [key, option]) => {
        acc[key] = option
        return acc
      }, {})

    this.setState(validate(values))
  }

  handleOnFocus = (event) => {
    const { name } = event.target

    const values = Object.entries(this.state.values)
      .map(([key, option]) => {
        option.focused = key === name
        if (key === name) {
          option.touched = true
        }
        return [key, option]
      })
      .reduce((acc, [key, option]) => {
        acc[key] = option
        return acc
      }, {})

    this.setState({values})
  }


  static propTypes = {
    title: PropTypes.string,
    values: PropTypes.array,
    order: PropTypes.object.isRequired,
    setOrder: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    submitTitle: PropTypes.string,
    isFetching: PropTypes.bool,
    reset: PropTypes.func,
    total: PropTypes.number,
    isValid: PropTypes.bool,
    isPaymentAllowed: PropTypes.bool,
    isSummaryShowed: PropTypes.bool,
    isCommentShowed: PropTypes.bool,
  }

  static defaultProps = {
    values: ['name', 'phone', 'comment', 'date'],
    submitTitle: 'Заказать торт',
    isFetching: false,
    isValid: true,
    isPaymentAllowed: true,
    isSummaryShowed: true,
    isCommentShowed: true,
  }

  constructor (props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.form = React.createRef();
  }

  componentDidMount () {
    this.props.setOrder({
      name: '',
      phone: '',
      comment: '',
      date: null
    })

    this.setState(validate(this.state.values))
  }

  handleSubmit (event) {
    event.preventDefault()
    const { values, id } = this.state
    const order = Object.entries(values)
      .reduce((acc, [key, item]) => {
        acc[key] = item.value
        return acc
      }, {})
    
    if (order.date instanceof Date) {
      order.date = dateFormat(order.date, 'dd MMMM')
    }

    order.delivery = this.state.delivery
    order.payment = this.state.payment

    const source = window
      ? window.sessionStorage.getItem('source')
      : null

    order.source = source

    this.props.submit(id, order, this.form.current)
  }

  render () {
    const {
      title,
      submitTitle,
      isFetching,
      children,
      total,
      isValid,
      isPaymentAllowed,
      isSummaryShowed,
      isCommentShowed
    } = this.props

    const {
      id,
      values,
      error,
      payment,
      delivery
    } = this.state

    const orderDeliveryPrice = delivery === 'city'
      ? deliveryPrice
      : 0

    const orderTotal = total + orderDeliveryPrice

    return (
      <>
        {isSummaryShowed ? (
          <OrderInfo
            title={title}
            total={orderTotal}
            deliveryPrice={orderDeliveryPrice}
            reset={this.handleOnReset}
            payment={payment}
            delivery={delivery}
            setDelivery={this.handleOnDeliveryChange}
            setPayment={this.handleOnPaymentChange}
            isPaymentAllowed={isPaymentAllowed}
          >
            {children}
          </OrderInfo>
        ) : (
          children
        )}
        <form
          ref={this.form}
          action={'https://money.yandex.ru/eshop.xml'}
          method="post"
          onSubmit={this.handleSubmit}
        >
          <input name="shopId" value={SHOP_ID} type="hidden" required />
          <input name="scid" value={SC_ID} type="hidden" required />
          <input name="customerNumber" type="hidden" value={id} size="64" required />
          <input name="sum" value={orderTotal} type="hidden" required />

          <div className={bem.b('OrderForm').e('line').show()}>

            <FormField
              label="Имя"
              isRequired={true}
              values={values}
              onFocus={this.handleOnFocus}
              onChange={this.handleOnChange}
              name="name"
              Component="input"
              type="text"
              autoComplete="off"
            />

            <FormField
              label="Телефон"
              isRequired={true}
              values={values}
              onFocus={this.handleOnFocus}
              onChange={this.handleOnChange}
              name="phone"
              Component={PhoneInput}
            />

            <FormField
              label="Когда приготовить"
              isRequired={true}
              values={values}
              onFocus={() => this.handleOnFocus({target: {name: 'date'}})}
              onChange={(value) => this.handleOnChange({target: {name: 'date', value}})}
              name="date"
              Component={DateInput}
            />

          </div>
          
          {isCommentShowed && (
            <div className={bem.b('OrderForm').e('line').show()}>
              <FormField
                label="Комментарий"
                values={values}
                onFocus={this.handleOnFocus}
                onChange={this.handleOnChange}
                name="comment"
                Component="textarea"
                autoComplete="off"
                />
            </div>
          )}

          <div className={bem.b('OrderForm').e('line').show()}>
            Заполняя данную форму, я даю свое согласие на обработку персональных данных
          </div>

          <div className={bem.b('OrderForm').e('line').m('submit').show()}>
            <button
              className={bem.b('OrderForm').e('submit').show()}
              disabled={error || isFetching || !isValid}
              type="submit"
            >
              {submitTitle}
            </button>
          </div>

        </form>
      </>
    );
  }
}

export { OrderForm }
