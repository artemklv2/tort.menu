import React from 'react'
import PropTypes from 'prop-types'
import BEM from '../../utils/bem'
import styles from './FormField.css'

const bem = new BEM(styles)

class FormField extends React.Component {
  static propTypes = {
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    values: PropTypes.object.isRequired,
    Component: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.func,
      PropTypes.instanceOf(React.Component)
    ]),
    onFocus: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired
  }

  static defaultProps = {
    isRequired: false
  }

  render () {
    const {
      label,
      values,
      name,
      Component,
      onChange,
      onFocus,
      isRequired,
      ...props
    } = this.props

    const {
      value,
      error,
      touched,
      focused
    } = values[name]

    const isErrorShown = error && !focused && touched

    return (
      <div className={bem.b('FormField').m('invalid', isErrorShown).show()}>
        {label && (
          <label className={bem.b('FormField').e('label').m('isRequired', isRequired).show()}>
            {label}
          </label>
        )}
        <Component
          value={value}
          onFocus={onFocus}
          onChange={onChange}
          name={name}
          {...props}
        />
        {isErrorShown && (
          <div className={bem.b('FormField').e('error').show()}>{error}</div>
        )}
      </div>
    )
  }
}

export default FormField
