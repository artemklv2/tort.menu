import React from 'react'
import PropTypes from 'prop-types'
import { BEM } from '../../utils/bem'
import styles from './OrderInfo.css'
import {
  OrderInfo as OrderInfoStyled,
  OrderInfoTitle,
  OrderInfoLine,
  OrderInfoLineValue,
  OrderInfoLineTitle,
  OrderInfoLineDescription,
  Checkbox
} from '../Elements'

const bem = new BEM(styles)

class OrderInfo extends React.Component {
  static propTypes = {
    reset: PropTypes.func.isRequired,
    delivery: PropTypes.oneOf([
      'pickup',
      'city'
    ]),
    payment: PropTypes.oneOf([
      'cash',
      'online'
    ]),
    setDelivery: PropTypes.func.isRequired,
    setPayment: PropTypes.func.isRequired,
    deliveryPrice: PropTypes.number.isRequired,
    isPaymentAllowed: PropTypes.bool,
    title: PropTypes.string,
  }

  static defaultProps = {
    title: 'Ваш заказ'
  }

  render () {
    const {
      deliveryPrice,
      delivery,
      setDelivery,
      payment,
      setPayment,
      children,
      reset,
      total,
      isPaymentAllowed,
      title
    } = this.props

    return (
      <OrderInfoStyled>
        <OrderInfoTitle>
          {title}
        </OrderInfoTitle>
        {children}
        <OrderInfoLine>
          <OrderInfoLineTitle>
            Доставка
          </OrderInfoLineTitle>
          <OrderInfoLineDescription>
            <Checkbox
              checked={delivery === 'pickup'}
              onChange={() => setDelivery('pickup')}
            >
              самовывоз
            </Checkbox>
            <Checkbox
              checked={delivery === 'city'}
              onChange={() => setDelivery('city')}
            >
              доставка по городу - 300р
            </Checkbox>
          </OrderInfoLineDescription>
          <OrderInfoLineValue>
            <div>{new Intl.NumberFormat('ru-RU').format(deliveryPrice)} &#8381;</div>
          </OrderInfoLineValue>
        </OrderInfoLine>

        <OrderInfoLine>
          <OrderInfoLineTitle>
            Способ оплаты
          </OrderInfoLineTitle>
          <OrderInfoLineDescription>
            <Checkbox
              checked={payment === 'cash'}
              onChange={() => setPayment('cash')}
            >
              оплата при получении
            </Checkbox>
            {isPaymentAllowed && (
              <Checkbox
                checked={payment === 'online'}
                onChange={() => setPayment('online')}
              >
                онлайн-оплата картой
              </Checkbox>
            )}
          </OrderInfoLineDescription>
          <OrderInfoLineValue>
            <div
              className={bem.b('OrderInfo').e('sumText').show()}
            >сумма</div>
            <div className={bem.b('OrderInfo').e('sumValue').show()}
            >{new Intl.NumberFormat('ru-RU').format(total)} &#8381;</div>
          </OrderInfoLineValue>
        </OrderInfoLine>

        <OrderInfoLine>
          <OrderInfoLineTitle />
          <OrderInfoLineDescription />
          <OrderInfoLineValue>
            <button
              className={bem.b('OrderInfo').e('reset').show()}
              onClick={reset}
            >Очистить</button>
          </OrderInfoLineValue>
        </OrderInfoLine>
      </OrderInfoStyled>
    )
  }
}

export { OrderInfo }
