import React from 'react'
import DatePicker, { registerLocale} from 'react-datepicker'
import ruRu from 'date-fns/locale/ru'
import addDays from 'date-fns/addDays'
import './DateInput.css'
import 'react-datepicker/dist/react-datepicker.css'


class DateTimeInput extends React.Component {
  state = {
    isShowed: false
  }

  componentDidMount () {
    registerLocale('ru', ruRu)
    this.setState({
      isShowed: true
    })
  }

  handleOnChange = (value) => {
    this.props.onChange({target: {value, name: 'date'}})
  }

  handleOnFocus = (value) => {
    this.props.onFocus({target: {value, name: 'date'}})
  }

  render () {
    const { isShowed } = this.state

    if (!isShowed) {
      return null
    }

    const {
      value,
      ...props
    } = this.props

    const selected = value instanceof Date
      ? value
      : null

    return (
      <DatePicker
        autoComplete="off"
        name="date"
        selected={selected}
        locale="ru"
        dateFormat="dd MMMM"
        minDate={new Date()}
        maxDate={addDays(new Date(), 30)}
        {...props}
      />
    )
  }
}

export default DateTimeInput
