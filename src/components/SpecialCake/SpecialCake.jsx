import React from 'react'
import { LayoutInner } from '../Layout'
import BEM from '../../utils/bem'
import styles from './SpecialCake.css'
import { FillingStep } from '../QuickCake/FillingStep'
import { Decor } from './Decor'
import { OrderForm } from '../OrderForm/OrderForm'
import { specialCakeFilling } from '../../data'
import { sendOrder } from '../../utils/api'
import { specialCakeDecor } from '../../data'
import { OrderResultHandler } from '../OrderResultHandler'
import { sendEvent } from '../../utils/yandex'
import {
  OrderInfoLine,
  OrderInfoLineTitle,
  OrderInfoLineDescription,
  OrderInfoLineValue,
} from '../Elements'

const bem = new BEM(styles)

function getFillingItem (filling) {
  return specialCakeFilling.find(item => item.id === filling)
}

function getTotal (fillingItem, decorItem) {
  let total = 0

  if (fillingItem) {
    total += fillingItem.price
  }

  if (decorItem) {
    total += decorItem.price
  }

  return total
}

class SpecialCake extends React.Component {
  state = {
    filling: undefined,
    decor: undefined,
    file: undefined,
    comment: '',
    order: {},
    hasError: false,
    isFetching: false,
    isSubmitted: false
  }

  constructor (props) {
    super(props)
    this.submit = this.submit.bind(this)
    this.setIsFetching = this.setIsFetching.bind(this)
    this.setIsSubmitted = this.setIsSubmitted.bind(this)
    this.setHasError = this.setHasError.bind(this)
  }

  setIsFetching (value) {
    return new Promise((resolve) => {
      this.setState({
        isFetching: value
      }, () => resolve(value))
    })
  }

  setIsSubmitted (value) {
    return new Promise((resolve) => {
      this.setState({
        isSubmitted: value
      }, () => resolve(value))
    })
  }

  setHasError (value) {
    return new Promise((resolve) => {
      this.setState({
        hasError: value
      }, () => resolve(value))
    })
  }

  setFilling = (filling) => {
    this.setState({
      filling
    })
  }
  
  setDecor = (decor) => {
    this.setState({
      decor
    })
  }

  setFile = (file) => {
    this.setState({
      file
    })
  }

  setOrder = (order) => {
    this.setState({
      order
    })
  }

  setComment = (event) => {
    this.setState({
      comment: event.target.value
    })
  }

  resetSpecialCake = () => {
    this.setState({
      filling: undefined,
      decor: undefined,
      file: undefined,
      comment: '',
      order: {},
      hasError: false,
      isFetching: false,
      isSubmitted: false
    })
  }

  async submit (id, order) {
    event.preventDefault()
    
    const payload = {}
    const {
      file,
      filling,
      decor,
      comment
    } = this.state

    sendEvent('orderosobenniitort')

    if (file) {
      payload.file = file
    }
    if (filling) {
      payload.filling = specialCakeFilling.find(item => item.id === filling)
    }
    if (decor) {
      payload.decor = specialCakeDecor.find(item => item.id === decor)
    }

    if (comment) {
      order.comment = comment
    }

    await this.setIsFetching(true)

    try {
      await sendOrder('special_cake', id, order, payload)
    } catch (error) {
      this.setHasError(true)
    } finally {
      await this.setIsFetching(false)
      await this.setIsSubmitted(true)
    }
  }

  render () {
    const {
      filling,
      decor,
      order,
      isFetching,
      hasError,
      isSubmitted,
      comment
    } = this.state

    const fillingItem = getFillingItem(filling)
    const total = getTotal(fillingItem)

    return (
      <LayoutInner>
        <OrderResultHandler
          isSubmitted={isSubmitted}
          hasError={hasError}
        >
          <div className={bem.b('SpecialCake').e('line').show()}>
            <FillingStep
              items={specialCakeFilling}
              filling={filling}
              setFilling={this.setFilling}
            />
            <Decor
              filling={filling}
              decor={decor}
              setDecor={this.setDecor}
              setFile={this.setFile}
            />
          </div>
          <div className={bem.b('SpecialCake').e('line').show()}>
            <div>
              <OrderForm
                title="Пожалуйста, заполните форму, менеджер свяжется с вами для согласования заказа"
                total={total}
                reset={this.resetSpecialCake}
                order={order}
                setOrder={this.setOrder}
                submit={this.submit}
                isFetching={isFetching}
                isPaymentAllowed={true}
                isCommentShowed={false}
              >
                <OrderInfoLine style={{display: 'block'}}>
                  <label className={bem.b('SpecialCake').e('orderCommentLabel').show()}>
                    дополнительные пожелания по украшению, к декору
                  </label>
                  <textarea
                    className={bem.b('SpecialCake').e('orderCommentText').show()}
                    value={comment}
                    onChange={this.setComment}
                  />
                </OrderInfoLine>
                <OrderInfoLine>
                  <OrderInfoLineTitle>начинка</OrderInfoLineTitle>
                  <OrderInfoLineDescription>
                    {Boolean(fillingItem) && (`${fillingItem.title}, ${fillingItem.weight} кг`)}
                  </OrderInfoLineDescription>
                  <OrderInfoLineValue>
                    {Boolean(fillingItem) && (
                      `${new Intl.NumberFormat('ru-RU').format(fillingItem.price)} ₽`
                    )}
                  </OrderInfoLineValue>
                </OrderInfoLine>
              </OrderForm>
            </div>
          </div>
        </OrderResultHandler>
      </LayoutInner>
    )
  }
}

export { SpecialCake } 
