import React from 'react'
import PropTypes from 'prop-types'
import Header from '../QuickCake/Header'
import { DecorVariants } from './DecorVariants'
import styles from './Decor.css'
import BEM from '../../utils/bem'
import { specialCakeFilling } from '../../data'

import decorStyles from '../QuickCake/DecorStep/DecorStep.css'

const dBem = new BEM(decorStyles)

const bem = new BEM(styles)

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class Decor extends React.Component {
  static propTypes = {
    decor: PropTypes.number,
    setDecor: PropTypes.func,
    setFile: PropTypes.func
  }

  handleOnFileChange = (event) => {
    const { setFile } = this.props
    if (!event.target.files.length) {
      return this.props.setFile(undefined)
    }

    const [file] = event.target.files
    getBase64(file)
      .then(data => setFile(data))
      .catch(err => setFile(undefined))
  }
  
  render () {
    const {
      filling,
      decor,
      setDecor,
      file,
      setFile
    } = this.props

    const fillingItem = specialCakeFilling.find(item => item.id === filling)

    return (
      <div>
        <Header>ШАГ 2 &ndash; Выбрать оформление</Header>

        <div className={dBem.b('DecorStep').e('blockItem').show()}>
          <div className={dBem.b('DecorStep').e('itemTitle').show()}>Вы выбрали начинку</div>
          <div className={dBem.b('DecorStep').e('itemValues').show()}>
            {!!fillingItem && (
              <span>
                {fillingItem.title} ({new Intl.NumberFormat('ru-RU').format(fillingItem.price)} ₽)
              </span>
            )}
          </div>
        </div>

        <h3 className={bem.b('QuickCakeDecor').e('subTitle').show()}>
          загрузите свой вариант торта
        </h3>
        <input 
          type="file"
          onChange={this.handleOnFileChange}
        />
        <h3 className={bem.b('QuickCakeDecor').e('subTitle').show()}>
          или выберите из нашей коллекции
        </h3>
        <DecorVariants
          decor={decor}
          setDecor={setDecor}
        />
      </div>
    )  
  }
}

export { Decor }
