import React from 'react'
import PropTypes from 'prop-types'
import styles from './CagetoryHeader.css'
import { LayoutInner } from '../Layout'

class CategoryHeader extends React.Component {
  static propTypes = {
    description: PropTypes.string.isRequired
  }

  render () {
    const {
      description
    } = this.props

    return (
      <LayoutInner>
        <div
          className={styles.CategoryHeader}
          dangerouslySetInnerHTML={{__html: description}}
        />
      </LayoutInner>
    )
  }
}

export { CategoryHeader }
