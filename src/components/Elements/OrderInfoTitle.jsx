import React from 'react'
import styles from './OrderInfoTitle.css'

function OrderInfoTitle ({children}) {
  return (
    <h2 className={styles.OrderInfoTitle}>
      {children}
    </h2>
  )
}

export { OrderInfoTitle }
