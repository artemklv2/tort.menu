import React from 'react'
import styles from './OrderInfo.css'

function OrderInfo ({children}) {
  return (
    <div className={styles.OrderInfo}>
      {children}
    </div>
  )
}

export { OrderInfo }
