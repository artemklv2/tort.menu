import React from 'react'
import styles from './Checkbox.css'
import BEM from '../../utils/bem'

const bem = new BEM(styles)

function Checkbox ({children, checked, ...props}) {
  return (
    <label className={bem.b('Checkbox').m('checked', checked).show()}>
      <input
        className={bem.b('Checkbox').e('input').show()}
        type="checkbox"
        checked={checked}
        {...props}
      />
      {children}
    </label>
  )
}

export { Checkbox }
