import React from 'react'
import styles from './OrderInfoLineDescription.css'

function OrderInfoLineDescription ({children}) {
  return (
    <div className={styles.OrderInfoLineDescription}>
      {children}
    </div>
  )
}

export { OrderInfoLineDescription }
