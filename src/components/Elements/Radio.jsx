import React from 'react'
import styles from './Radio.css'
import BEM from '../../utils/bem'

const bem = new BEM(styles)

function Radio ({children, checked, ...props}) {
  return (
    <label className={bem.b('Radio').m('checked', checked).show()}>
      <input
        className={bem.b('Radio').e('input').show()}
        type="radio"
        checked={checked}
        {...props}
      />
      {children}
    </label>
  )
}

export { Radio }
