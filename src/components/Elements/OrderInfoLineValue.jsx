import React from 'react'
import styles from './OrderInfoLineValue.css'

function OrderInfoLineValue ({children}) {
  return (
    <div className={styles.OrderInfoLineValue}>
      {children}
    </div>
  )
}

export { OrderInfoLineValue }
