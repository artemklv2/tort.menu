import React from 'react'
import styles from './OrderInfoLineTitle.css'

function OrderInfoLineTitle ({children}) {
  return (
    <div className={styles.OrderInfoLineTitle}>
      <div>
        {children}
      </div>
    </div>
  )
}

export { OrderInfoLineTitle }
