import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './OrderInfoLine.css'

const bem = new BEM(styles)

function OrderInfoLine ({children, className, style = {}}) {
  return (
    <div style={style} className={`${bem.b('OrderInfoLine').show()} ${className}`}>
      {children}
    </div>
  )
}

export { OrderInfoLine }
