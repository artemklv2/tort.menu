import React from 'react'
import PropTypes from 'prop-types'
import BEM from '../../utils/bem'
import styles from './TagList.css'

const bem = new BEM(styles)

class TagItem extends React.Component {
  static propTypes = {
    tag: PropTypes.number,
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    setTag: PropTypes.func.isRequired
  }

  handleOnClick = () => {
    const {
      id,
      setTag
    } = this.props

    setTag(id)
  }

  render () {
    const { name, id, tag } = this.props

    return (
      <button
        onClick={this.handleOnClick}
        className={bem.b('TagList').e('item').m('isActive', tag === id).show()}
      >
        {name}
      </button>
    )
  }
}

class TagList extends React.Component {
  static propTypes = {
    tags: PropTypes.array.isRequired,
    setTag: PropTypes.func.isRequired,
    tag: PropTypes.number
  }

  render () {
    const { tags, tag, setTag } = this.props

    return (
      <div className={bem.b('TagList').show()}>
        {tags.map(item => (
          <TagItem
            key={`tag${item.id}`}
            tag={tag}
            setTag={setTag}
            {...item}
          />
        ))}
      </div>
    )
  }
}

export { TagList }
