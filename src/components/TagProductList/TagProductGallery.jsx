import React from 'react'
import PropTypes from 'prop-types'
import Lightbox from 'react-images'
import styles from './TagProductGallery.css'

class GalleryThumbItem extends React.Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    setImage: PropTypes.func.isRequired,
    thumbnail: PropTypes.string.isRequired
  }

  handleOnClick = (event) => {
    event.preventDefault()

    const {
      index,
      setImage
    } = this.props

    setImage(index)
  }

  render () {
    const { thumbnail } = this.props

    return (
      <a
        className={styles.TagProductGallery__item}
        onClick={this.handleOnClick}
        href="#"
      >
        <img src={thumbnail} />
      </a>
    )
  }
}

class TagProductGallery extends React.Component {
  static propTypes = {
    images: PropTypes.array.isRequired
  }

  state = {
    currentImageIndex: 0,
    isOpen: false
  }

  openImage = (index) => {
    this.setState({
      currentImageIndex: index,
      isOpen: true
    })
  }

  closeImage = () => {
    this.setState({
      currentImageIndex: 0,
      isOpen: false
    })
  }

  gotoNext = () => {
    const {
      currentImageIndex
    } = this.state

    const {
      images
    } = this.props

    const index = (currentImageIndex < images.length - 1)
      ? currentImageIndex + 1
      : 0

    this.setState({
      currentImageIndex: index
    })
  }

  gotoPrev = () => {
    const {
      currentImageIndex
    } = this.state

    const {
      images
    } = this.props

    const index = (currentImageIndex > 0)
      ? currentImageIndex - 1
      : images.length

    this.setState({
      currentImageIndex: index
    })
  }

  render () {
    const {
      currentImageIndex,
      isOpen
    } = this.state

    const {
      images
    } = this.props

    return (
      <div className={styles.TagProductGallery}>
        <div className={styles.TagProductGallery__inner}>
          {images.map((image, index) => (
            <GalleryThumbItem
              key={`image${image.id}`}
              setImage={this.openImage}
              index={index}
              thumbnail={image.thumbnail}
            />
          ))}
        </div>
        <Lightbox
          currentImage={currentImageIndex}
          images={images}
          isOpen={isOpen}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrev}
          onClose={this.closeImage}
        />
      </div>
    )
  }
}

export { TagProductGallery }
