import React from 'react'
import PropTypes from 'prop-types'
import { TagList } from './TagList'
import {
  IMG_PATH_ORIGIN,
  IMG_PATH_THUMBNAIL
} from '../../constants/rootConstants'
import { TagProductGallery } from './TagProductGallery'
import { LayoutInner } from '../Layout'

function getAllImages (products) {
  return products
    .filter(product => product.images.length)
    .map(product => ({
      id: product.id,
      src: `${IMG_PATH_ORIGIN}${product.images[0]}`,
      thumbnail: `${IMG_PATH_THUMBNAIL}${product.images[0]}`,
      tags: product.taxons
    }))
}

function filterImages (images, tag) {
  return images
    .filter(image => image.tags.includes(tag))
}

class TagProductList extends React.Component {
  static propTypes = {
    tags: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    limit: PropTypes.number,
  }

  static defaultProps = {
    limit: 1000
  }

  state = {
    tag: undefined,
    images: [],
    allImages: []
  }

  setTag = (newTag) => {
    const tag = newTag === this.state.tag
      ? undefined
      : newTag

    const images = tag
      ? filterImages(this.state.allImages, tag)
      : this.state.allImages

    this.setState({
      tag,
      images
    })
  }

  componentDidMount () {
    const allImages = getAllImages(this.props.products).splice(1, this.props.limit);

    this.setState({
      allImages,
      images: allImages
    })
  }

  render () {
    const { tags } = this.props
    const { tag, images } = this.state

    return (
      <LayoutInner>
        <TagList
          tag={tag}
          tags={tags}
          setTag={this.setTag}
        />
        <TagProductGallery images={images} />
      </LayoutInner>
    )
  }
}

export { TagProductList }
