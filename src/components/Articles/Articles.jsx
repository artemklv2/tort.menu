import React from 'react'
import PropTypes from 'prop-types'
import { LayoutInner } from '../Layout'
import styles from './Articles.css'

function Articles ({articles}) {
  return (
    <LayoutInner>
      <div className={styles.Articles}>
        {articles.map(({title, text}, index) => (
          <setion
            key={`article_${index}`} 
            className={styles.Articles__item}
          >
            <h2 dangerouslySetInnerHTML={{__html: title}} />
            <p dangerouslySetInnerHTML={{__html: text}} />
          </setion>
        ))}
      </div>
    </LayoutInner>
  )
}

Articles.propTypes = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      text: PropTypes.string
    })
  )
}

Articles.defaultProps = {
  articles: []
}

export { Articles }
