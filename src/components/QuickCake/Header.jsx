import React from 'react'
import styles from './Header.css'

const Header = ({children}) => (
  <h3 className={styles.QuickCakeHeader}>{children}</h3>
)

export default Header
