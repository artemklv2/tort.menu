import React from 'react'
import PropTypes from 'prop-types'
import styles from './Step.css'
import BEM from '../../utils/bem'

const bem = new BEM(styles)

class StepItem extends React.Component {
  static propTypes = {
    step: PropTypes.number.isRequired,
    disabled: PropTypes.bool.isRequired,
    currentStep: PropTypes.number.isRequired,
    setStep: PropTypes.func
  }

  handleOnClick = () => {
    const { step, setStep } = this.props
    setStep(step)
  }

  render () {
    const { children, currentStep, step, disabled } = this.props
    return (
      <button
        className={bem.b('Step').e('item').m('isActive', currentStep === step).show()}
        onClick={this.handleOnClick}
        disabled={disabled}
      >
        {children}
      </button>
    )
  }
}

class Step extends React.Component {
  static propTypes = {
    step: PropTypes.number.isRequired,
    isStepOneValid: PropTypes.bool.isRequired,
    isStepTwoValid: PropTypes.bool.isRequired,
    setStep: PropTypes.func.isRequired
  }

  render () {
    const { step, setStep, isStepOneValid, isStepTwoValid } = this.props

    return (
      <div className={bem.b('Step').show()}>
        <StepItem step={1} currentStep={step} disabled={false} setStep={setStep}>
          выбрать начинку
        </StepItem>
        <div className={bem.b('Step').e('delimiter').show()} />
        <StepItem step={2} currentStep={step} disabled={!isStepOneValid} setStep={setStep}>
          выбрать оформление
        </StepItem>
        <div className={bem.b('Step').e('delimiter').show()} />
        <StepItem step={3} currentStep={step} disabled={!isStepTwoValid} setStep={setStep}>
          отправить заявку
        </StepItem>
      </div>
    )
  }
}

export { Step }
