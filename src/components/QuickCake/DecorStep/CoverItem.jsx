import React from 'react'
import PropTypes from "prop-types";
import {Radio} from "../../Elements";
import { Checkbox } from '../../Elements/Checkbox';

class CoverItem extends React.Component {
  static propTypes = {
    currentCover: PropTypes.number,
    setCover: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
  }

  handleOnChange = () => {
    const {
      currentCover,
      id
    } = this.props

    if (id === currentCover) {
      this.props.setCover(undefined)
    } else {
      this.props.setCover(this.props.id)
    }
  }

  render () {
    const {id, title, currentCover} = this.props
    return (
      <Checkbox
        onChange={this.handleOnChange}
        value={id}
        checked={id === currentCover}
      >
        {title}
      </Checkbox>
    )
  }
}

export { CoverItem }
