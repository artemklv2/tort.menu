import React from 'react'
import PropTypes from 'prop-types'
import styles from './ColorItem.css'
import BEM from '../../../utils/bem'

const bem = new BEM(styles)

class ColorItem extends React.Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    currentColor: PropTypes.number,
    title: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    setColor: PropTypes.func.isRequired
  }

  handleOnColorChange = () => {
    this.props.setColor(this.props.id)
  }

  render () {
    const {id, currentColor, color, title} = this.props

    return (
      <button
        title={title}
        style={{
          backgroundColor: color
        }}
        className={bem.b('ColorItem').m('isActive', id === currentColor).show()}
        onClick={this.handleOnColorChange}
      />
    )
  }
}

export { ColorItem }
