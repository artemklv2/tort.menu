import React from 'react'
import PropTypes from 'prop-types'
import { DecorVariants } from './DecorVariants'
import styles from './DecorStep.css'
import BEM from '../../../utils/bem'
import Header from '../Header'

const bem = new BEM(styles)

class DecorStep extends React.Component {
  static propTypes = {
    cover: PropTypes.number,
    setCover: PropTypes.func
  }

  handleOnTestChange = (event) => {
    this.props.setText(event.target.value)
  }

  render () {
    const {
      text,
      decor,
      setDecor,
    } = this.props


    return (
      <div className={bem.b('DecorStep').show()}>
        <Header>ШАГ 2 &ndash; Выбрать оформление</Header>
        <div className={bem.b('DecorStep').e('block').show()}>

          <DecorVariants decor={decor} setDecor={setDecor} />

          {/* Надпись */}
          <div className={bem.b('DecorStep').e('blockItem').show()}>
            <div className={bem.b('DecorStep').e('itemTitle').show()}>Надпись на торте</div>
            <div className={bem.b('DecorStep').e('itemValues').show()}>
              <div className={bem.b('DecorStep').e('textWrapper').show()}>
                <input
                  value={text}
                  className={bem.b('DecorStep').e('textInput').show()}
                  onChange={this.handleOnTestChange}
                />
                <div className={bem.b('DecorStep').e('texCount').show()}>
                  <span>{text ? text.length : 0}</span>
                  /
                  <span>{text ? (25 - text.length) : 25}</span>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    )
  }
}

export { DecorStep }
