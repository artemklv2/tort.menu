import React from 'react'
import PropTypes from 'prop-types'
import { quickCakeDecor as decorList } from '../../../data'
import styles from './DecorVariants.css'
import BEM from '../../../utils/bem'

const bem = new BEM(styles)

class DecorItem extends React.Component {
  state = {
    isShowed: false
  }

  static propTypes = {
    setDecor: PropTypes.func.isRequired,
    currentDecor: PropTypes.number,
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string
  }

  handleOnClick = () => {
    this.props.setDecor(this.props.id)
  }

  handleOnMouseEnter = () => {
    this.setState({
      isShowed: true
    })
  }

  handleOnMouseLeave = () => {
    this.setState({
      isShowed: false
    })
  }

  render () {
    const { 
      currentDecor, 
      id, 
      image, 
      description
    } = this.props

    const {
      isShowed
    } = this.state
    
    return (
      <button
        onClick={this.handleOnClick}
        className={bem.b('DecorVariants').e('item').m('isActive', id === currentDecor).show()}
        onMouseEnter={this.handleOnMouseEnter}
        onMouseLeave={this.handleOnMouseLeave}
      >
        <img
          src={`/static/images/decor/${image}`}
          className={bem.b('DecorVariants').e('itemImage').show()}
        />
        <p className={bem.b('DecorVariants').e('itemDescription').m('isShowed', isShowed).show()}>
          <span>
            {description}
          </span>
        </p>
      </button>
    )
  }
}

class DecorVariants extends React.Component {
  static propTypes = {
    decor: PropTypes.number,
    setDecor: PropTypes.func.isRequired
  }

  render () {
    const {
      setDecor,
      decor
    } = this.props

    return (
      <div className={bem.b('DecorVariants').show()}>
        <div className={bem.b('DecorVariants').e('list').show()}>
          {decorList.map(item => (
            <DecorItem
              key={`decor${item.id}`}
              setDecor={setDecor}
              currentDecor={decor}
              {...item}
            />))}
        </div>
      </div>
    )
  }
}

export { DecorVariants }
