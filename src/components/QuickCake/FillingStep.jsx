import React from 'react'
import PropTypes from 'prop-types'
import Header from './Header'
import BEM from '../../utils/bem'
import styles from './FillingStep.css'

const bem = new BEM(styles)

class FillingItem extends React.Component {
  state = {
    isShowed: false
  }

  static propTypes = {
    filling: PropTypes.number,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    setFilling: PropTypes.func.isRequired,
    isCake: PropTypes.bool,
  }

  static defaultProps = {
    isCake: false
  }

  handleOnClick = () => {
    const {id, setFilling} = this.props
    setFilling(id)
  }

  handleOnMouseEnter = () => {
    this.setState({
      isShowed: true
    })
  }

  handleOnMouseLeave = () => {
    this.setState({
      isShowed: false
    })
  }

  render () {
    const {
      id,
      filling,
      title,
      description,
      image,
      price,
      weight,
      isCake
    } = this.props

    const {
      isShowed
    } = this.state

    return (
      <button
        onClick={this.handleOnClick}
        className={
          bem
            .b('FillingStep')
            .e('item')
            .m('isActive', id === filling)
            .m('isCake', isCake)
            .show()
          }
        onMouseEnter={this.handleOnMouseEnter}
        onMouseLeave={this.handleOnMouseLeave}
      >
        <span className={
          bem
            .b('FillingStep')
            .e('innerBlock')
            .m('isCake', isCake)
            .show()
          }>
          <p className={
            bem
              .b('FillingStep')
              .e('description')
              .m('isShowed', isShowed)
              .m('isCake', isCake)
              .show()
            }
          >
            <span>
              {description}
            </span>
          </p>
          <img
            src={`/static/images/filling/${image}`}
            className={bem.b('FillingStep').e('image').show()}
          />
        </span>
        <span className={bem.b('FillingStep').e('title').show()}>
          {title}
        </span>
        <span className={bem.b('FillingStep').e('price').show()}>
          {new Intl.NumberFormat('ru-RU').format(price)} ₽ / {weight}кг.
        </span>
      </button>
    )
  }
}

class FillingStep extends React.Component {
  static propTypes = {
    filling: PropTypes.number,
    setFilling: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired
  }

  render () {
    const {
      items,
      filling, 
      setFilling
    } = this.props

    return (
      <div className={bem.b('FillingStep').show()}>
        <Header>ШАГ 1 &ndash; Выбрать начинку</Header>
        <div className={bem.b('FillingStep').e('inner').show()}>
          {items.map(item => <FillingItem
            key={`filling${item.id}`}
            filling={filling}
            setFilling={setFilling}
            {...item}
          />)}
        </div>
      </div>
    )
  }
}

export { FillingStep }
