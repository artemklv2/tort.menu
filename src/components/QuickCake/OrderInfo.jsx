import React from 'react'
import PropTypes from 'prop-types'
import styles from './OrderInfo.css'
import { 
  filling as fillingList,
  cover as coverList 
} from './data'

function OrderInfo ({
  filling,
  cover,
  color,
  meringue,
  waffle,
  berries,
  text,
  decor
}) {
  if (
    !filling &&
    !cover &&
    !color &&
    !meringue &&
    !waffle &&
    !berries.length &&
    !text &&
    !decor
  ) {
    return null
  }

  const fillingItem = fillingList.find(item => item.id === filling)
  const coverItem = coverList.find(item => item.id === cover)
  const berriesList = [{id: 1, title: 'клубника'}, {id: 2, title: 'голубика'}]
    .filter(item => berries.includes(item.id))
    .map(item => item.title)
    .join(', ')

  return (
    <div className={styles.OrderInfo}>
      <h3 className={styles.OrderInfo__title}>Ваш заказ</h3>
      <p>
        {!!fillingItem && (
          <>
            <b>начинка</b>
            <span> &ndash; {fillingItem.title} ({new Intl.NumberFormat('ru-RU').format(fillingItem.price)} ₽), </span>
          </>
        )}
        {!!coverItem && (
          <>
            <b>покрытие</b>
            <span> &ndash; {coverItem.title}, </span>
          </>
        )}
        {!!color && (
          <>
            <b>цвет</b>
            <span> &ndash; {color}, </span>
          </>
        )}
        {!!meringue && (
          <span>безе (куполочки/леденцы), </span>
        )}
        {!!waffle && (
          <span>вафельный рожок, </span>
        )}
        {!!berriesList.length && (
          <>
            <b>свежие ягоды</b>
            &nbsp;
            <span>+300р/100г</span>
            <span> &ndash; {berriesList}, </span>
          </>
        )}
        {!!text && (
          <>
            <b>надпись на торте</b>
            <span> &ndash; {text} </span>
          </>
        )}
        {!!decor && (
          <>
            <b>оформление</b>
            <span> &ndash; из нашей коллекции</span>
          </>
        )}
      </p>
    </div>
  )
}

OrderInfo.propTypes = {
  filling: PropTypes.number,
  cover: PropTypes.number,
  color: PropTypes.string,
  decor: PropTypes.number,
  decoration: PropTypes.string,
  text: PropTypes.string,
  berries: PropTypes.array,
  waffle: PropTypes.string,
  meringue: PropTypes.string
}

export { OrderInfo }
