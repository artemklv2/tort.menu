import React from 'react'
import { LayoutInner } from '../Layout'
import { FillingStep } from './FillingStep'
import { DecorStep } from './DecorStep'
import BEM from '../../utils/bem'
import styles from './QuickCake.css'
import { OrderForm } from '../OrderForm'
import { quickCakeFilling, quickCakeDecor } from '../../data'
import { sendOrder } from '../../utils/api'
import { OrderResultHandler } from '../OrderResultHandler'
import { sendEvent } from '../../utils/yandex'
import { OrderInfoLine, OrderInfoLineDescription, OrderInfoLineTitle, OrderInfoLineValue } from '../Elements';

const bem = new BEM(styles)

function getTotal (fillingItem, decorItem) {
  let total = 0

  if (fillingItem) {
    total += fillingItem.price
  }

  if (decorItem) {
    total += decorItem.price
  }

  return total
}

function getFillingItem (filling) {
  return quickCakeFilling.find(item => item.id === filling)
}

function getDecorItem (decor) {
  return quickCakeDecor.find(item => item.id === decor)
}

class QuickCake extends React.Component {
  state = {
    filling: undefined,
    decor: undefined,
    text: '',
    order: {},
    isFetching: false,
    isSubmitted: false,
    hasError: false
  }

  constructor (props) {
    super(props)
    this.submit = this.submit.bind(this)
    this.setIsFetching = this.setIsFetching.bind(this)
    this.setIsSubmitted = this.setIsSubmitted.bind(this)
    this.setHasError = this.setHasError.bind(this)
  }

  setIsFetching (value) {
    return new Promise((resolve) => {
      this.setState({
        isFetching: value
      }, () => resolve(value))
    })
  }

  setIsSubmitted (value) {
    return new Promise((resolve) => {
      this.setState({
        isSubmitted: value
      }, () => resolve(value))
    })
  }

  setHasError (value) {
    return new Promise((resolve) => {
      this.setState({
        hasError: value
      }, () => resolve(value))
    })
  }

  resetQuickCake = () => {
    this.setState({
      filling: undefined,
      decor: undefined,
      text: '',
    })
  }

  setFilling = (newFilling) => {
    const { decor } = this.state
    const fillingItem = getFillingItem(newFilling)
    const isCake = typeof fillingItem === 'object'
      ? Boolean(fillingItem.isCake)
      : false
    const newDecor = isCake
      ? undefined
      : decor
    this.setState({
      filling: newFilling,
      decor: newDecor
    })
  }

  setText = (text) => {
    this.setState({
      text: text.substring(0, 25)
    })
  }

  setDecor = (decor) => {
    const { filling } = this.state
    const fillingItem = getFillingItem(filling)
    const isCake = Boolean(filling)
      ? Boolean(fillingItem.isCake)
      : false
    if (isCake) {
      return
    }
    this.setState({decor})
  }

  setOrder = (order) => {
    this.setState({
      order
    })
  }

  async submit (id, contact, form) {
    const {
      order,
      isFetching,
      isSubmitted,
      hasError,
      ...payload
    } = this.state

    sendEvent('orderbistriitort')
    
    try {
      if (payload.filling) {
        payload.filling = quickCakeFilling.find(item => item.id === payload.filling)
      }
      if (payload.decor) {
        payload.decor = quickCakeDecor.find(decor => decor.id === payload.decor)
      }

      await this.setIsFetching(true)

      try {
        await sendOrder('quick_cake', id, contact, payload)
        if (contact.payment === 'online') {
          form.submit()
        }
      } catch (error) {
        console.log(error)
        this.setHasError(true)
      } finally {
        await this.setIsFetching(false)
        await this.setIsSubmitted(true)
      }
    } catch (error) {
      console.log(error)
      this.setHasError(true)
    }
  }

  render () {
    const {
      order,
      filling,
      text,
      decor,
      isFetching,
      hasError,
      isSubmitted
    } = this.state

    const fillingItem = getFillingItem(filling)
    const isCake = Boolean(filling)
      ? Boolean(fillingItem.isCake)
      : false

    const decorItem = getDecorItem(decor)
    const isValid = isCake 
      ? Boolean(filling)
      : Boolean(filling) && Boolean(decor)

    const total = getTotal(fillingItem, decorItem)

    return (
      <LayoutInner>
        <OrderResultHandler
          hasError={hasError}
          isSubmitted={isSubmitted}
        >
          <div className={bem.b('QuickCake').e('line').show()}>
            <FillingStep
              items={quickCakeFilling}
              filling={filling}
              setFilling={this.setFilling}
            />
            <DecorStep
              text={text}
              setText={this.setText}
              decor={decor}
              setDecor={this.setDecor}
            />
          </div>
          <div className={bem.b('QuickCake').e('line').show()}>
            <div>
              <OrderForm
                total={total}
                reset={this.resetQuickCake}
                order={order}
                setOrder={this.setOrder}
                submit={this.submit}
                isFetching={isFetching}
                isValid={isValid}
              >
                <OrderInfoLine>
                  <OrderInfoLineTitle>
                    {isCake ? 'торт' : 'начинка'}
                  </OrderInfoLineTitle>
                  <OrderInfoLineDescription>
                    {Boolean(fillingItem) && (`${fillingItem.title}, ${fillingItem.weight} кг`)}
                  </OrderInfoLineDescription>
                  <OrderInfoLineValue>
                    {Boolean(fillingItem) && (
                      `${new Intl.NumberFormat('ru-RU').format(fillingItem.price)} ₽`
                    )}
                  </OrderInfoLineValue>
                </OrderInfoLine>
                {(isCake === false) && (
                  <OrderInfoLine>
                    <OrderInfoLineTitle>оформление</OrderInfoLineTitle>
                    <OrderInfoLineDescription>
                      {Boolean(decorItem) && (decorItem.description)}
                    </OrderInfoLineDescription>
                    <OrderInfoLineValue>
                      {Boolean(decorItem) && (
                        `${new Intl.NumberFormat('ru-RU').format(decorItem.price)} ₽`
                      )}
                    </OrderInfoLineValue>
                  </OrderInfoLine>
                )}
              </OrderForm>
            </div>
          </div>
        </OrderResultHandler>
      </LayoutInner>
    )
  }
}

export { QuickCake }
