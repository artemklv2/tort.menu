const cover = [
  {
    id: 1,
    title: 'Сливками',
  },
  {
    id: 2,
    title: 'Мастикой'
  }
]

const color = [
  {
    id: 1,
    title: 'Желтый',
    color: '#ffef03'
  },
  {
    id: 2,
    title: 'Красный',
    color: '#E42022'
  },
  {
    id: 3,
    title: 'Оранжевый',
    color: '#EE7F1C'
  },
  {
    id: 4,
    title: 'Синий',
    color: '#393085'
  },
  {
    id: 5,
    title: 'Коричневый',
    color: '#897870'
  },
  {
    id: 6,
    title: 'Розовый',
    color: '#CA5499'
  },
  {
    id: 7,
    title: 'Зеленый',
    color: '#009848'
  }
]

const decoration = [
  {
    id: 1,
    title: 'Клубника'
  },
  {
    id: 2,
    title: 'Глубика'
  }
]

export {
  filling,
  decor,
  cover,
  color,
  decoration
}
