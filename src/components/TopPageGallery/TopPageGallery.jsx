import React from 'react'
import { topPageGallery } from '../../data'
import Slider from 'react-slick'
import BEM from '../../utils/bem'
import styles from './TopPageGallery.css'

const bem = new BEM(styles)

function TopPageGallery () {
  return (
    <div className={bem.b('TopPageGallery').show()}>
      <div className={bem.b('TopPageGallery').e('tip').show()}>
        <p className={bem.b('TopPageGallery').e('tipInner').show()}>
          ЛЮБИМЫЕ ТОРТИКИ ЛЮБИМЫМ 
        </p>
      </div>

      <Slider
        autoplay={true}
        dots={false}
        fade={true}
        autoplaySpeed={2000}
        speed={2000}
      >
        {topPageGallery.map((item, index) => (
          <img key={`img${index}`} src={item.original} />
        ))}
      </Slider>
    </div>
  )
}

export {
  TopPageGallery
}
