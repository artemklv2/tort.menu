import React from 'react'
import styles from './TopPageLogos.css'
import { sendEvent } from '../../utils/yandex'

function TopPageLogos () {
  return (
    <section className={styles.TopPageLogos}>
      <p className={styles.TopPageLogos__item}>
        <a
          href="http://maccheroniciao.ru/"
          className={styles.TopPageLogos__itemLogo}
          onClick={() => sendEvent('maccheroniciao')}
        >
          <img
            alt="Maccheroni" 
            src="/static/images/logos/maccheroni.png"
          />
        </a>
        <span className={styles.TopPageLogos__itemAddress}>Ленина, 40</span>
        <a 
          href="tel:7-343-222-1902"
          className={styles.TopPageLogos__itemPhone}
          onClick={() => sendEvent('telmaccheroniciao')}
        ></a>
      </p>

      <p className={styles.TopPageLogos__item}>
        <a
          href="http://mammas.ru/"
          className={styles.TopPageLogos__itemLogo}
          onClick={() => sendEvent('mammas')}
        >
          <img
            alt="Mamma's big House" 
            src="/static/images/logos/mammas.png"
          />
        </a>
        <span className={styles.TopPageLogos__itemAddress}>Ленина, 26</span>
        <a 
          href="tel:7-343-222-1905"
          className={styles.TopPageLogos__itemPhone}
          onClick={() => sendEvent('telmammas')}
        ></a>
      </p>

      <p className={styles.TopPageLogos__item}>
        <a
          href="http://donnaolivia.ru/"
          className={styles.TopPageLogos__itemLogo}
          onClick={() => sendEvent('donnaolivia')}
        >
          <img
            alt="Donna Olivia" 
            src="/static/images/logos/donna.png"
          />
        </a>
        <span className={styles.TopPageLogos__itemAddress}>8 Марта, 41</span>
        <a 
          href="tel:7-343-222-1900"
          className={styles.TopPageLogos__itemPhone}
          onClick={() => sendEvent('teldonnaolivia')}
        ></a>
      </p>

      <p className={styles.TopPageLogos__item}>
        <a
          href="http://donnaolivia.ru/"
          className={styles.TopPageLogos__itemLogo}
          onClick={() => sendEvent('donnaoliviamega')}
        >
          <img
            alt="Donna Olivia and Mega" 
            src="/static/images/logos/mega.png"
          />
        </a>
        <span className={styles.TopPageLogos__itemAddress}>ТЦ Мега, фудкорт</span>
        <a 
          href="tel:7-343-222-1903"
          className={styles.TopPageLogos__itemPhone}
          onClick={() => sendEvent('teldonnaoliviamega')}
        ></a>
      </p>
    </section>
  )
}

export {
  TopPageLogos
}
