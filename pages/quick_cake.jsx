import React from 'react'
import { CategoryHeader } from '../src/components/CategoryHeader'
import { Layout } from '../src/components/Layout'
import {
  getCategory,
  getProductsByCategory
} from '../src/utils/data'
import { SimpleProductList } from '../src/components/SimpleProductList'
import { readyCakeDescription } from '../src/data'

class KupitGotoviiTortPage extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      resp.category = await getCategory('quick_cake')
      resp.products = await getProductsByCategory(resp.category.id)
    } catch (e) {
      resp.error = true
    }

    resp.pathname = asPath

    return resp
  }

  render () {
    const {
      error,
      products,
      pathname
    } = this.props


    return (
      <Layout pathname={pathname}>
        {error && (
          <p>Что-то пошло не так</p>
        )}
        {!error && (
          <>
            <CategoryHeader description={readyCakeDescription} />
            <SimpleProductList products={products} parentUrl="/quick_cake" />
          </>
        )}
      </Layout>
    )
  }
}

export default KupitGotoviiTortPage
