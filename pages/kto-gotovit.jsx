import React from 'react'
import { Layout, LayoutInner } from '../src/components/Layout'
import styles from './kto-gotovit.css'
import { TopPageLogos } from '../src/components/TopPageLogos'
import { GallerySlider } from '../src/components/GallerySlider'
import { cooksGallery } from '../src/data'

class WhoOrder extends React.Component {
  static async getInitialProps ({asPath}) {
    return {
      pathname: asPath
    }
  }

  render () {
    const { pathname } = this.props

    return (
      <Layout pathname={pathname}>
        <LayoutInner>
          <div className="YouTube">
          <iframe src="https://www.youtube.com/embed/vlxMq7MS57A"
            frameborder="0" allow="accelerometer; 
            autoplay; encrypted-media; gyroscope; 
            picture-in-picture" allowfullscreen></iframe>
          </div>
          <GallerySlider items={cooksGallery} />
          <article className={styles.WhoOrder}>
            <div className={styles.WhoOrder__inner}>
              <img src="/static/images/who-cake.jpg" />
              <p>
              Почему все любят наши тортики? Потому что у нас вкусные рецепты. 
              Потому что мы используем только качественные и натуральные продукты, 
              обходимся без консервантов, печём на современном оборудовании и любим свое дело. 
              Получается вкусно и красиво!
              </p>
            </div>
          </article>
        </LayoutInner>
        <TopPageLogos />
      </Layout>
    )
  }
}

export default WhoOrder
