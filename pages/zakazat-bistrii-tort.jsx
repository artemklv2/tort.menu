import React from 'react'
import { Layout } from '../src/components/Layout'
import { CategoryHeader } from '../src/components/CategoryHeader'
import { QuickCake } from '../src/components/QuickCake'
import { quickCakeDescription } from '../src/data'

class QuickCakePage extends React.Component {
  static async getInitialProps ({asPath}) {
    return {
      pathname: asPath
    }
  }

  render () {
    const { pathname } = this.props

    return (
      <Layout pathname={pathname}>
        <CategoryHeader description={quickCakeDescription} />
        <QuickCake/>
      </Layout>
    )
  }
}

export default QuickCakePage
