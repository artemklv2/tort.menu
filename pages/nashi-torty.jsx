import React from 'react'
import { Layout } from '../src/components/Layout'
import { CategoryHeader } from '../src/components/CategoryHeader'
import { getCategory, getProductsByCategory } from '../src/utils/data'
import { TagProductList } from '../src/components/TagProductList'

class NashiTorty extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      resp.category = await getCategory('our_cake')
      resp.products = await getProductsByCategory(resp.category.id)
    } catch (e) {
      resp.error = true
    }

    resp.pathname = asPath

    return resp
  }

  render () {
    const {
      category,
      products,
      pathname
    } = this.props

    return (
      <Layout pathname={pathname}>
        <CategoryHeader description={category.description} />
        <TagProductList
          tags={category.children}
          products={products}
        />
      </Layout>
    )
  }
}

export default NashiTorty
