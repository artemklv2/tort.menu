import React from 'react'
import { Layout } from '../src/components/Layout'
import { getProductData } from '../src/utils/data'
import { ProductPage } from '../src/components/ProductPage'

class Sladosti extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      const code = asPath.split('/').pop()
      resp.product = await getProductData(code)
    } catch (e) {
      resp.error = true

    }

    resp.pathname = asPath

    return resp
  }

  render () {
    const {
      product,
      pathname
    } = this.props

    return (
      <Layout pathname={pathname}>
        <ProductPage product={product} />
      </Layout>
    )
  }
}

export default Sladosti
