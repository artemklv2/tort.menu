import React from 'react'
import { Layout } from '../src/components/Layout'
import { CategoryHeader } from "../src/components/CategoryHeader";
import { SpecialCake } from '../src/components/SpecialCake'
import { specialCakeDescription } from '../src/data'

class SpecialCakePage extends React.Component {
  static async getInitialProps ({asPath}) {
    return {
      pathname: asPath
    }
  }

  render () {
    const { pathname } = this.props

    return (
      <Layout pathname={pathname}>
        <CategoryHeader description={specialCakeDescription} />
        <SpecialCake />
      </Layout>
    )
  }
}

export default SpecialCakePage
