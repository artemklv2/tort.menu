import React from 'react'
import { Layout } from '../src/components/Layout'
import { Articles } from '../src/components/Articles'
import { howOrder } from '../src/data'
import { TopPageLogos } from '../src/components/TopPageLogos'

class HowOrder extends React.Component {
  static async getInitialProps ({asPath}) {
    return {
      pathname: asPath
    }
  }

  render () {
    const {
      pathname
    } = this.props

    return (
      <Layout pathname={pathname}>
        <Articles articles={howOrder} />
        <TopPageLogos />
      </Layout>
    )
  }
}

export default HowOrder
