import React from 'react'
import { Layout, LayoutInner } from '../src/components/Layout'
import { CategorySlider } from '../src/components/CategorySlider'
import styles from './index.css'
import {TagProductList} from '../src/components/TagProductList'
import { TopPageGallery } from '../src/components/TopPageGallery/TopPageGallery'
import { getCategory, getProductsByCategory } from '../src/utils/data'
import { TopPageLogos } from '../src/components/TopPageLogos'
import { TopPageCooks } from '../src/components/TopPageCooks'
import { TopPageCakes } from '../src/components/TopPageCakes'

class IndexPage extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      resp.quickCakeCategory = await getCategory('quick_cake')
      resp.sladostiCategory = await getCategory('sladosti')
      resp.sladosti = await getProductsByCategory(resp.sladostiCategory.id)
      resp.ourCakeCategory = await getCategory('our_cake')
      resp.ourCake = await getProductsByCategory(resp.ourCakeCategory.id)
      resp.newYearCategory = await getCategory('novogodnie-torty')
      resp.newYear = await getProductsByCategory(resp.newYearCategory.id)
    } catch (error) {
      console.log(error.message)
      resp.error = true
    }

    resp.pathname = asPath

    return resp
  }

	render () {
    const { 
      sladosti, 
      ourCake,
      ourCakeCategory,
      pathname
     } = this.props


		return (
			<Layout pathname={pathname}>
        <TopPageGallery />

        <TopPageCooks />

        <LayoutInner>
          <section>
            <h3 className={styles.IndexPage__title}>- Мы можем предложить Вам -</h3>
          </section>
        </LayoutInner>

        <TopPageCakes />

        <LayoutInner>
          <section>
            <h3 className={styles.IndexPage__title}>- НАШИ ТОРТИКИ -</h3>
          </section>
        </LayoutInner>

        <TagProductList
          tags={ourCakeCategory.children}
          products={ourCake}
          limit={8}
        />

        <LayoutInner>
          <section>
            <h3 className={styles.IndexPage__title}>- СЛАДОСТИ -</h3>
            <p className={styles.IndexPage__desctiption}>наши кондитеры приготовили конфеты ручной работы, домашний зефир, капкейки и печенье</p>
          </section>
        </LayoutInner>
        <CategorySlider products={sladosti} parentUrl="/sladosti"/>


        <LayoutInner>
          <section>
            <h3 className={styles.IndexPage__title}>- Попробывать и заказать наши тортики можно в ресторанах - 
            <p> +7(929)204-80-53</p></h3>
          </section>
        </LayoutInner>
        <TopPageLogos />

      </Layout>
		)
	}
}

export default IndexPage
