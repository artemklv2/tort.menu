import React from 'react'
import { Layout, LayoutInner } from '../src/components/Layout'
import { CardList } from '../src/components/Card'

class Card extends React.Component {
  static async getInitialProps ({asPath}) {
    return {
      pathname: asPath
    }
  }

  render () {
    const {
      pathname
    } = this.props

    return (
      <Layout pathname={pathname}>
        <LayoutInner>
          <CardList/>
        </LayoutInner>
      </Layout>
    )
  }
}

export default Card
