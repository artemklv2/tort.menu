const withCSS = require('@zeit/next-css')
const { fetch } = require('cross-fetch')

async function getProductsPathMap (categories) {
  const products = require('./data/products')
  return products
    .filter(product => (
      product.category &&
      categories.includes(product.category.code)
    ))
    .reduce((acc, product) => {
      acc[`/${product.category.code}/${product.code}`] = {
        page: '/product',
        query: `?title=${product.code}`
      }
      return acc
    }, {})
}

module.exports = withCSS({
	cssModules: true,
	cssLoaderOptions: {
		importLoaders: 1,
		localIdentName: "[local]",
	},
  exportPathMap: async function (defaultPathMap) {
    const productPages = await getProductsPathMap([
      'sladosti',
      'quick_cake',
      'novogodnie-torty'
    ])

    delete defaultPathMap['/product']

	  return {
      ...defaultPathMap,
      ...productPages
    }
  }
})
